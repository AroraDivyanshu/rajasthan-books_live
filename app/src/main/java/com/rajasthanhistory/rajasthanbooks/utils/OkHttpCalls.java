package com.rajasthanhistory.rajasthanbooks.utils;

import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;

import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by wscubetech on 4/10/16.
 */
public class OkHttpCalls {

    String strUrl = "";
    ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();

    Call call;

    public OkHttpCalls(String strUrl, ArrayList<KeyValueModel> arrayKeyValueModel) {
        this.strUrl = strUrl;
        this.arrayKeyValueModel = arrayKeyValueModel;
    }

    public void initiateCall(Callback callback) {
        HttpUrl.Builder builder = HttpUrl.parse(strUrl).newBuilder();
        for (KeyValueModel model : arrayKeyValueModel)
            builder.addQueryParameter(model.getKey(), model.getValue());

        String url = builder.build().toString();

        Request request = new Request.Builder().url(url).build();

        OkHttpClient client = new OkHttpClient();

        call=client.newCall(request);

        call.enqueue(callback);

    }

    public Call getCall(){
        return call;
    }

}
