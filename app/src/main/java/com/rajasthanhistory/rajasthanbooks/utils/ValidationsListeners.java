package com.rajasthanhistory.rajasthanbooks.utils;

import android.app.Activity;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.rajasthanhistory.rajasthanbooks.R;


/**
 * Created by wscubetech on 16/6/16.
 */
public class ValidationsListeners {

    Activity activity;
    MyValidations validations;

    public ValidationsListeners(Activity activity) {
        this.activity = activity;
        validations = new MyValidations(activity);
    }

    public void inputOperationError(TextInputLayout inp, EditText et, String msg) {
        inp.setErrorEnabled(true);
        inp.setError(msg);
        et.requestFocus();
    }

    public void inputOperationNoError(TextInputLayout inp) {
        inp.setErrorEnabled(false);
        inp.setError(null);
    }

    public class MyTextWatcherInputLayout implements TextWatcher {

        TextInputLayout inpLayout;
        EditText etPassword;

        public MyTextWatcherInputLayout(TextInputLayout inpLayout) {
            this.inpLayout = inpLayout;
        }

        public MyTextWatcherInputLayout(TextInputLayout inpLayout, EditText etPassword) {
            this(inpLayout);
            this.etPassword = etPassword;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            String input = editable.toString().trim();
            switch (inpLayout.getId()) {
                case R.id.inpFirstName:
                    if (input.length() > 0 && validations.checkName(input))
                        inputOperationNoError(inpLayout);
                    break;
                case R.id.inpPhone:
                    if (validations.checkMobile(input))
                        inputOperationNoError(inpLayout);
                    break;
                case R.id.inpEmail:
                    if (validations.checkEmail(input))
                        inputOperationNoError(inpLayout);
                    break;

                case R.id.inpPassword:
                    if (input.length() > 2)
                        inputOperationNoError(inpLayout);
                    break;

                case R.id.inpConfirmPassword:
                    String pass = etPassword.getText().toString();
                    if (pass.equals(input))
                        inputOperationNoError(inpLayout);
                    break;
            }
        }
    }

}
