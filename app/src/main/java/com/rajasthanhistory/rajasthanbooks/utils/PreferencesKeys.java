package com.rajasthanhistory.rajasthanbooks.utils;

/**
 * Created by wscubetech on 5/10/16.
 */
public class PreferencesKeys {

    public static final String PREFERENCE_SETTINGS = "BookSettings";

    public static final String PREF_SLIDER_HOME = "SliderHome";
    public static final String PREF_FEATURED_HOME = "FeaturedHome";
    public static final String PREF_NEW_HOME = "NewHome";
    public static final String PREF_CATEGORY_HOME = "CategoryHome";

    public static final String PREF_FEATURED_ALL = "Featured";
    public static final String PREF_NEW_ALL = "New";
    public static final String PREF_SALE_ALL = "Sale";
    public static final String PREF_FREE_ALL = "Free";

    public static final String PREF_USER_FIRST_NAME = "UserFirstName";
    public static final String PREF_USER_LAST_NAME = "UserLastName";
    public static final String PREF_USER_EMAIL = "UserEmail";
    public static final String PREF_USER_PHONE = "UserPhone";
    public static final String PREF_USER_PASSWORD = "UserPassword";
    public static final String PREF_USER_ID = "UserId";

    public static final String PREF_MY_BOOKS = "MyBooks";

    public static final String PREF_BOOK_INDEX = "BookIndex_";

}
