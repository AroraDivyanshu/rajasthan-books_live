package com.rajasthanhistory.rajasthanbooks.utils;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;

/**
 * Created by wscubetech on 23/10/16.
 */
public class PayPalConfig {
    // PayPal app configuration
    public static final String PAYPAL_CLIENT_ID_SANDBOX="AUGVRkZ1WebhJ3IzJLlm5ybcNz35LCuLv8v1Ol2JtT_Zctpf5RVe-ItJOSFSsn4YZOLWdihJa6pv_hXM";
    public static final String PAYPAL_CLIENT_ID_LIVE="Ae9fQ71atJxIDelgFlO-YPjqTyq9MR5cM3H414hBfLSiuaclm-w7TxSI2JFV7pXRxsQaNE-OJYVUKgth";

    //PayPal account
    //wscubetechtesting@gmail.com  Password:wscubetechapps$
    public static final String PAYPAL_CLIENT_ID_ = "AdF0IIb3FqDN2BaPRMyXVBOSah4lIPuj6Glnx19hsvfcITzDsjk3c6TWLWPlsfLcKwe2eRUOocXIcXby";

    public static final String PAYPAL_CLIENT_SECRET = "";

    public static final String PAYPAL_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    public static final String PAYMENT_INTENT = PayPalPayment.PAYMENT_INTENT_SALE;
    public static final String DEFAULT_CURRENCY = "USD";

    // Our php+mysql server urls
    public static final String URL_PRODUCTS = "http://192.168.0.104/PayPalServer/v1/products";
    public static final String URL_VERIFY_PAYMENT = "http://192.168.0.104/PayPalServer/v1/verifyPayment";

}
