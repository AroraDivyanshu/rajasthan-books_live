package com.rajasthanhistory.rajasthanbooks.utils;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.rajasthanhistory.rajasthanbooks.R;

import java.io.File;

/**
 * Created by wscubetech on 18/10/16.
 */
public class DownloadFile {

    Activity activity;
    File file;

    public DownloadFile(Activity activity) {
        this.activity = activity;
    }

    public void startDownloading(String url) {
        Log.v("Url", url);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setTitle(activity.getString(R.string.app_name));
        request.setDescription("Downloading e-book");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        String path = Environment.getDataDirectory() + "/data/" + activity.getPackageName() + "/files/";
        String fileName = url.substring(url.lastIndexOf("/") + 1);
        request.setDestinationInExternalFilesDir(activity, path, fileName);

        Log.v("Path", path + fileName);
        file = new File(path);
        if (file.exists())
            Log.v("ActualFilePath", file.getAbsolutePath());

        DownloadManager downloadManager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
        downloadManager.enqueue(request);
    }


}
