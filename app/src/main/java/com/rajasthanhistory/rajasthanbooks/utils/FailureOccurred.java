package com.rajasthanhistory.rajasthanbooks.utils;

import android.app.Activity;
import android.view.View;

import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;

/**
 * Created by wscubetech on 4/10/16.
 */
public class FailureOccurred {

    Activity activity;
    DialogMsg dialogMsg;

    public FailureOccurred(Activity activity) {
        this.activity = activity;
    }

    public void showFailureDialog(final boolean finishActivity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialogMsg = new DialogMsg(activity);
                dialogMsg.showDialog(activity.getString(R.string.network_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogMsg.getDialog().dismiss();

                        if (finishActivity)
                            activity.finish();
                    }
                });
            }
        });
    }

}
