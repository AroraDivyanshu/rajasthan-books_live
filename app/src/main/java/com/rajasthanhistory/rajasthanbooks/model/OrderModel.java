package com.rajasthanhistory.rajasthanbooks.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wscubetech on 19/10/16.
 */
public class OrderModel implements Serializable {

    String orderId = "", orderCodeName = "", orderTotalAmount = "", orderPaymentType = "", orderPaymentStatus = "", orderDate = "";
    ArrayList<BookModel> arrayBookModel = new ArrayList<>();

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderCodeName() {
        return orderCodeName;
    }

    public void setOrderCodeName(String orderCodeName) {
        this.orderCodeName = orderCodeName;
    }

    public String getOrderTotalAmount() {
        return orderTotalAmount;
    }

    public void setOrderTotalAmount(String orderTotalAmount) {
        this.orderTotalAmount = orderTotalAmount;
    }

    public String getOrderPaymentType() {
        return orderPaymentType;
    }

    public void setOrderPaymentType(String orderPaymentType) {
        this.orderPaymentType = orderPaymentType;
    }

    public String getOrderPaymentStatus() {
        return orderPaymentStatus;
    }

    public void setOrderPaymentStatus(String orderPaymentStatus) {
        this.orderPaymentStatus = orderPaymentStatus;
    }

    public ArrayList<BookModel> getArrayBookModel() {
        return arrayBookModel;
    }

    public void setArrayBookModel(ArrayList<BookModel> arrayBookModel) {
        this.arrayBookModel = arrayBookModel;
    }
}
