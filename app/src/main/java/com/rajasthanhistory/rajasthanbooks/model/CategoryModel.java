package com.rajasthanhistory.rajasthanbooks.model;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wscubetech on 30/9/16.
 */
public class CategoryModel implements ParentListItem,Serializable {
    String categoryId="",categoryName="",categoryPhoto="";
    ArrayList<SubCategoryModel> arraySubCategoryModel=new ArrayList<>();

    public String getCategoryPhoto() {
        return categoryPhoto;
    }

    public void setCategoryPhoto(String categoryPhoto) {
        this.categoryPhoto = categoryPhoto;
    }

    public ArrayList<SubCategoryModel> getArraySubCategoryModel() {
        return arraySubCategoryModel;
    }

    public void setArraySubCategoryModel(ArrayList<SubCategoryModel> arraySubCategoryModel) {
        this.arraySubCategoryModel = arraySubCategoryModel;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public List<?> getChildItemList() {
        return arraySubCategoryModel;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
