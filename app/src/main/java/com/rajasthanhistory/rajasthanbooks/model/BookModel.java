package com.rajasthanhistory.rajasthanbooks.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by wscubetech on 1/10/16.
 */
public class BookModel implements Serializable {
    String bookId="",bookTitle="",bookPrice="",bookDiscountedPrice="",bookAuthor="",bookAuthorAbout="",bookIndex="",bookDescription="",bookCode="",bookSize="",bookUrlFileName="",bookLanguage="";
    String bookImage="",bookType="1"/*1-> Ebook    2->Hard Copy*/;
    String availableOrOutOfStock=""; //0->Available   1->Out of stock
    String cartId="";
    String categoryId="",categoryName="";
    boolean wished=false;
    float bookAvgRating=0;

    ArrayList<String> arrayBookImageModel=new ArrayList<>();

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBookLanguage() {
        return bookLanguage;
    }

    public void setBookLanguage(String bookLanguage) {
        this.bookLanguage = bookLanguage;
    }

    public String getBookUrlFileName() {
        return bookUrlFileName;
    }

    public void setBookUrlFileName(String bookUrlFileName) {
        this.bookUrlFileName = bookUrlFileName;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public boolean isWished() {
        return wished;
    }

    public void setWished(boolean wished) {
        this.wished = wished;
    }

    public String getAvailableOrOutOfStock() {
        return availableOrOutOfStock;
    }

    public void setAvailableOrOutOfStock(String availableOrOutOfStock) {
        this.availableOrOutOfStock = availableOrOutOfStock;
    }

    public String getBookType() {
        return bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    public String getBookAuthorAbout() {
        return bookAuthorAbout;
    }

    public void setBookAuthorAbout(String bookAuthorAbout) {
        this.bookAuthorAbout = bookAuthorAbout;
    }

    public float getBookAvgRating() {
        return bookAvgRating;
    }

    public void setBookAvgRating(float bookAvgRating) {
        this.bookAvgRating = bookAvgRating;
    }

    public ArrayList<String> getArrayBookImageModel() {
        return arrayBookImageModel;
    }

    public void setArrayBookImageModel(ArrayList<String> arrayBookImageModel) {
        this.arrayBookImageModel = arrayBookImageModel;
    }

    public String getBookImage() {
        return bookImage;
    }

    public void setBookImage(String bookImage) {
        this.bookImage = bookImage;
    }

    public String getBookDiscountedPrice() {
        return bookDiscountedPrice;
    }

    public void setBookDiscountedPrice(String bookDiscountedPrice) {
        this.bookDiscountedPrice = bookDiscountedPrice;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getBookIndex() {
        return bookIndex;
    }

    public void setBookIndex(String bookIndex) {
        this.bookIndex = bookIndex;
    }

    public String getBookDescription() {
        return bookDescription;
    }

    public void setBookDescription(String bookDescription) {
        this.bookDescription = bookDescription;
    }

    public String getBookCode() {
        return bookCode;
    }

    public void setBookCode(String bookCode) {
        this.bookCode = bookCode;
    }

    public String getBookSize() {
        return bookSize;
    }

    public void setBookSize(String bookSize) {
        this.bookSize = bookSize;
    }


    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(String bookPrice) {
        this.bookPrice = bookPrice;
    }
}
