package com.rajasthanhistory.rajasthanbooks.model;

/**
 * Created by wscubetech on 1/10/16.
 */
public class SliderModel {
    String sliderId="",sliderAppImage="",sliderTitle="";
    String sliderFlag="";
    String sliderBookOrCatId="";

    public String getSliderCategoryName() {
        return sliderCategoryName;
    }

    public void setSliderCategoryName(String sliderCategoryName) {
        this.sliderCategoryName = sliderCategoryName;
    }

    String sliderCategoryName="";
    /*slider_flag=0,1,2,3,4
    0=>Free Book,
    1=>Featured Book,
    2=>Sale Book,
    3=>Book,
    4=>Category*/

    public String getSliderId() {
        return sliderId;
    }

    public void setSliderId(String sliderId) {
        this.sliderId = sliderId;
    }

    public String getSliderAppImage() {
        return sliderAppImage;
    }

    public void setSliderAppImage(String sliderAppImage) {
        this.sliderAppImage = sliderAppImage;
    }

    public String getSliderTitle() {
        return sliderTitle;
    }

    public void setSliderTitle(String sliderTitle) {
        this.sliderTitle = sliderTitle;
    }

    public String getSliderFlag() {
        return sliderFlag;
    }

    public void setSliderFlag(String sliderFlag) {
        this.sliderFlag = sliderFlag;
    }

    public String getSliderBookOrCatId() {
        return sliderBookOrCatId;
    }

    public void setSliderBookOrCatId(String sliderBookOrCatId) {
        this.sliderBookOrCatId = sliderBookOrCatId;
    }
}
