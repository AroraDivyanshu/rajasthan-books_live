package com.rajasthanhistory.rajasthanbooks.model;

import java.io.Serializable;

/**
 * Created by wscubetech on 30/9/16.
 */
public class SubCategoryModel implements Serializable {
    String subcategoryId="",subCategoryName="",subCategoryPhoto="";

    public String getSubCategoryPhoto() {
        return subCategoryPhoto;
    }

    public void setSubCategoryPhoto(String subCategoryPhoto) {
        this.subCategoryPhoto = subCategoryPhoto;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }
}
