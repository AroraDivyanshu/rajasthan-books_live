package com.rajasthanhistory.rajasthanbooks.model;

/**
 * Created by wscubetech on 28/10/16.
 */
public class BookIndex {
    int pageNumber=0;
    String title="";

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
