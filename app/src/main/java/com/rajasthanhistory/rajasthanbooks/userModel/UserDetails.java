package com.rajasthanhistory.rajasthanbooks.userModel;

import android.content.Context;

import com.rajasthanhistory.rajasthanbooks.utils.GetSetSharedPrefs;
import com.rajasthanhistory.rajasthanbooks.utils.PreferencesKeys;

/**
 * Created by wscubetech on 6/10/16.
 */
public class UserDetails {

    Context context;
    GetSetSharedPrefs prefs;

    public UserDetails(Context context) {
        this.context = context;
        prefs = new GetSetSharedPrefs(context, PreferencesKeys.PREFERENCE_SETTINGS);
    }

    public User getUserDetails() {
        User user = new User();
        user.setUserId(prefs.getData(PreferencesKeys.PREF_USER_ID));
        user.setUserFirstName(prefs.getData(PreferencesKeys.PREF_USER_FIRST_NAME));
        user.setUserLastName(prefs.getData(PreferencesKeys.PREF_USER_LAST_NAME));
        user.setUserEmail(prefs.getData(PreferencesKeys.PREF_USER_EMAIL));
        user.setUserPhone(prefs.getData(PreferencesKeys.PREF_USER_PHONE));
        user.setUserPassword(prefs.getData(PreferencesKeys.PREF_USER_PASSWORD));
        return user;
    }

    public void setUserDetails(User user) {
        prefs.putData(PreferencesKeys.PREF_USER_ID, user.getUserId());
        prefs.putData(PreferencesKeys.PREF_USER_FIRST_NAME, user.getUserFirstName());
        prefs.putData(PreferencesKeys.PREF_USER_LAST_NAME, user.getUserLastName());
        prefs.putData(PreferencesKeys.PREF_USER_EMAIL, user.getUserEmail());
        prefs.putData(PreferencesKeys.PREF_USER_PHONE, user.getUserPhone());
        prefs.putData(PreferencesKeys.PREF_USER_PASSWORD, user.getUserPassword());
    }

}
