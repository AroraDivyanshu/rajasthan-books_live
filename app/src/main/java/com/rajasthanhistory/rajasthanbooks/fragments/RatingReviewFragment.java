package com.rajasthanhistory.rajasthanbooks.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.rajasthanhistory.rajasthanbooks.BookData.EmptyScreen;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.BookDetailActivity;
import com.rajasthanhistory.rajasthanbooks.adapter.RatingReviewAdapter;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogRatingReview;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.model.RatingReviewModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.FailureOccurred;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by wscubetech on 7/10/16.
 */
public class RatingReviewFragment extends Fragment implements View.OnClickListener {

    RecyclerView recyclerView;
    String bookId = "";
    ProgressWheel progressWheel;

    FloatingActionButton fabAdd;

    ArrayList<RatingReviewModel> arrayRatingReviewModel = new ArrayList<>();
    RatingReviewAdapter adapter;

    DialogMsg dialogMsg, dialogWait;
    User user;

    boolean active, isUserRatingPresent = false;

    public RatingReviewFragment() {

    }

    public static Fragment newInstance(String bookId) {
        Fragment fragment = new RatingReviewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("BookId", bookId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rating_reviews, container, false);
        init(view);
        view.setOnClickListener(this);
        return view;
    }

    private void init(View v) {
        progressWheel = (ProgressWheel) v.findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        fabAdd = (FloatingActionButton) v.findViewById(R.id.fabAdd);
        fabAdd.setVisibility(View.GONE);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
        bookId = this.getArguments().getString("BookId");
        user = new UserDetails(getActivity()).getUserDetails();

        adapter = new RatingReviewAdapter(BookDetailActivity.bookDetailActivity, arrayRatingReviewModel, bookId);
        recyclerView.setAdapter(adapter);

        if (user.getUserId().equals("0"))
            fabAdd.setVisibility(View.GONE);

        okHttpViewRatings(!user.getUserId().equals("0"));
        onClickListeners();
    }

    @Override
    public void onStop() {
        active = false;
        super.onStop();
    }

    private void onClickListeners() {
        fabAdd.setOnClickListener(this);
    }

    private void okHttpViewRatings(final boolean myReview) {
        if (progressWheel.getVisibility() == View.GONE)
            progressWheel.setVisibility(View.VISIBLE);
        final ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("product_id", bookId));
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        Log.v("UserAndBook", user.getUserId() + "\n" + bookId);

        String url = myReview ? Urls.API_VIEW_USER_RATING : Urls.API_VIEW_RATING_REVIEW;

        OkHttpCalls okHttpCalls = new OkHttpCalls(url, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                BookDetailActivity.bookDetailActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressWheel.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {
                    final String strResponse = Html.fromHtml(response.body().string()).toString();
                    Log.v("Response", strResponse);
                    BookDetailActivity.bookDetailActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject json = new JSONObject(strResponse);
                                if (myReview) {
                                    arrayRatingReviewModel.clear();
                                    if (json.getInt("result") == 1) {
                                        isUserRatingPresent = true;
                                        JSONObject obj = json.getJSONObject("Message");
                                        RatingReviewModel model = new RatingReviewModel();
                                        model.setId(obj.getString("comment_id"));
                                        model.setComment(obj.getString("comment_message"));
                                        model.setRating(Float.parseFloat(obj.getString("comment_rating")));
                                        model.setDateTime(obj.getString("comment_time"));
                                        model.setUserId(obj.getString("user_id"));
                                        model.setUserName(obj.getString("user_fname"));
                                        arrayRatingReviewModel.add(model);
                                    } else {
                                        isUserRatingPresent = false;
                                    }

                                    okHttpViewRatings(false);
                                } else {
                                    if (json.getInt("result") == 1) {
                                        JSONArray array = json.getJSONArray("Message");
                                        for (int i = 0; i < array.length(); i++) {
                                            JSONObject obj = array.getJSONObject(i);
                                            RatingReviewModel model = new RatingReviewModel();
                                            model.setId(obj.getString("comment_id"));
                                            model.setComment(obj.getString("comment_message"));
                                            model.setRating(Float.parseFloat(obj.getString("comment_rating")));
                                            model.setDateTime(obj.getString("comment_time"));
                                            model.setUserId(obj.getString("user_id"));
                                            model.setUserName(obj.getString("user_fname"));
                                            arrayRatingReviewModel.add(model);
                                        }

                                    } else {
                                        if (active && !isUserRatingPresent) {
                                            EmptyScreen emptyScreen = new EmptyScreen(BookDetailActivity.bookDetailActivity);
                                            emptyScreen.setIconAndMsg("No ratings or reviews posted yet", R.drawable.ic_empty_star);
                                        }
                                    }
                                }

                                fabAdd.setVisibility(isUserRatingPresent ? View.GONE : View.VISIBLE);

                            } catch (Exception e) {

                            }
                        }
                    });
                }

                BookDetailActivity.bookDetailActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (progressWheel.getVisibility() == View.VISIBLE)
                            progressWheel.setVisibility(View.GONE);

                        if (active)
                            adapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    /*private String timeFormat(String date) {
        Date da;
        String parsedDate = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            da = sdf.parse(date);
            sdf = new SimpleDateFormat("MMM dd, yyyy");
            parsedDate = sdf.format(da);
            return parsedDate;
        } catch (ParseException e) {
            Log.v("DateParse", "" + e);
        }
        return "" + new Date();
    }*/

    private void showRatingReviewDialog() {
        final DialogRatingReview dialogRatingReview = new DialogRatingReview(BookDetailActivity.bookDetailActivity);
        dialogRatingReview.showRatingReviewDialog(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogRatingReview.getRating() == 0) {
                    Toast.makeText(BookDetailActivity.bookDetailActivity, "Please provide rating to submit", Toast.LENGTH_LONG).show();
                    return;
                }
                dialogRatingReview.getDialog().dismiss();
                if (!new ConnectionDetector(BookDetailActivity.bookDetailActivity).isConnectingToInternet()) {
                    dialogMsg = new DialogMsg(BookDetailActivity.bookDetailActivity);
                    dialogMsg.showDialog(getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogMsg.getDialog().dismiss();
                        }
                    });
                    return;
                }

                okHttpAddUpdateRating(dialogRatingReview.getRating(), dialogRatingReview.getReview());

            }
        }, 0, "");

    }

    private void okHttpAddUpdateRating(float rating, String msg) {

        dialogWait = new DialogMsg(BookDetailActivity.bookDetailActivity);
        dialogWait.showPleaseWaitDialog();


        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        arrayKeyValueModel.add(new KeyValueModel("product_id", bookId));
        arrayKeyValueModel.add(new KeyValueModel("comment_rating", rating + ""));
        Log.v("rating", rating+"");
        arrayKeyValueModel.add(new KeyValueModel("comment_message", msg));
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_ADD_UPDATE_RATING, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                BookDetailActivity.bookDetailActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogWait.getDialog().dismiss();
                    }
                });
                FailureOccurred failureOccurred = new FailureOccurred(BookDetailActivity.bookDetailActivity);
                failureOccurred.showFailureDialog(true);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    Log.v("Response", strResponse);
                    try {
                        JSONObject jsonObject = new JSONObject(strResponse);
                        if (jsonObject.getInt("result") == 1) {
                            BookDetailActivity.bookDetailActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (active) {
                                        fabAdd.setVisibility(View.GONE);
                                        dialogMsg = new DialogMsg(BookDetailActivity.bookDetailActivity);
                                        dialogMsg.showDialog("Rating successfully submitted", R.drawable.ic_success, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                dialogMsg.getDialog().dismiss();
                                                EmptyScreen emptyScreen = new EmptyScreen(BookDetailActivity.bookDetailActivity);
                                                emptyScreen.hideEmpty();
                                                okHttpViewRatings(true);
                                            }
                                        });
                                    }
                                }
                            });

                        }
                    } catch (Exception e) {
                        Log.v("Exception", "" + e);
                    }
                } else {
                    BookDetailActivity.bookDetailActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (active) {
                                dialogMsg = new DialogMsg(BookDetailActivity.bookDetailActivity);
                                dialogMsg.showDialog(getString(R.string.network_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialogMsg.getDialog().dismiss();
                                    }
                                });
                            }

                        }
                    });
                }

                BookDetailActivity.bookDetailActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active)
                            dialogWait.getDialog().dismiss();
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAdd:
                showRatingReviewDialog();
                break;
        }

    }

}
