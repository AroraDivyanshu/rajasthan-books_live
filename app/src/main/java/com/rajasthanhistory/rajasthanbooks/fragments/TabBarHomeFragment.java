package com.rajasthanhistory.rajasthanbooks.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.utils.GetSetSharedPrefs;
import com.rajasthanhistory.rajasthanbooks.utils.PreferencesKeys;
import com.rajasthanhistory.rajasthanbooks.utils.ViewPagerAdapter;

/**
 * Created by wscubetech on 3/10/16.
 */
public class TabBarHomeFragment extends Fragment implements View.OnClickListener {

    TabLayout tabLayout;
    public static ViewPager viewPager;

    GetSetSharedPrefs prefs;

    public TabBarHomeFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tabular_home, container, false);
        init(view);
        view.setOnClickListener(this);
        return view;
    }

    private void init(View v) {
        tabLayout = (TabLayout) v.findViewById(R.id.tabLayout);
        viewPager = (ViewPager) v.findViewById(R.id.viewPager);
    }

    @Override
    public void onStart() {
        super.onStart();
        setupViewPager();
    }


    public void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(HomeFragment.newInstance(), "Home");
        adapter.addFragment(FeaturedSaleFreeNewFragment.newInstance(PreferencesKeys.PREF_NEW_ALL), PreferencesKeys.PREF_NEW_ALL);
        adapter.addFragment(FeaturedSaleFreeNewFragment.newInstance(PreferencesKeys.PREF_FEATURED_ALL), PreferencesKeys.PREF_FEATURED_ALL);
        adapter.addFragment(FeaturedSaleFreeNewFragment.newInstance(PreferencesKeys.PREF_SALE_ALL), PreferencesKeys.PREF_SALE_ALL);
        adapter.addFragment(FeaturedSaleFreeNewFragment.newInstance(PreferencesKeys.PREF_FREE_ALL), PreferencesKeys.PREF_FREE_ALL);
        adapter.addFragment(new CategoriesFragment(), "Categories");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

    }


    @Override
    public void onClick(View view) {

    }


}
