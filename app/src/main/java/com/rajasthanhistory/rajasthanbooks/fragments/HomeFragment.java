package com.rajasthanhistory.rajasthanbooks.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.rajasthanhistory.rajasthanbooks.BookData.BookJsonParse;
import com.rajasthanhistory.rajasthanbooks.BookData.CategorySubCategoryParse;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.MainActivity;
import com.rajasthanhistory.rajasthanbooks.adapter.BooksHomePageAdapter;
import com.rajasthanhistory.rajasthanbooks.adapter.CategorySubCategoryAdapter;
import com.rajasthanhistory.rajasthanbooks.adapter.SliderAdapter;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.CategoryModel;
import com.rajasthanhistory.rajasthanbooks.model.SliderModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.FailureOccurred;
import com.rajasthanhistory.rajasthanbooks.utils.GetSetSharedPrefs;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;
import com.rajasthanhistory.rajasthanbooks.utils.PreferencesKeys;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by wscubetech on 1/10/16.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    ViewPager viewPager;
    SliderAdapter sliderAdapter;
    ArrayList<SliderModel> arraySliderModel = new ArrayList<>();

    RecyclerView recyclerViewNewRelease, recyclerViewFeatured, recyclerViewCategories;
    TextView txtMoreNewRelease, txtMoreFeatured, txtMoreCategories;
    ProgressWheel progressNewRelease, progressFeatured, progressCategories, progressWheel;

    LinearLayout linFeatured, linNewRelease, linCategories, linParent;

    LinearLayoutManager linearLayoutManager;
    BooksHomePageAdapter adapter;
    ArrayList<BookModel> arrayBookModel = new ArrayList<>();


    ArrayList<CategoryModel> arrayCategoryModel = new ArrayList<>();
    CategorySubCategoryAdapter categorySubCategoryAdapter;

    //DialogMsg dialogMsg;

    boolean active;

    GetSetSharedPrefs prefs;


    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        init(view);
        view.setOnClickListener(this);
        return view;
    }

    private void init(View v) {
        linParent = (LinearLayout) v.findViewById(R.id.linParent);
        progressWheel = (ProgressWheel) v.findViewById(R.id.progressBar);

        viewPager = (ViewPager) v.findViewById(R.id.viewPager);
        recyclerViewNewRelease = (RecyclerView) v.findViewById(R.id.recyclerViewNewRelease);
        recyclerViewFeatured = (RecyclerView) v.findViewById(R.id.recyclerViewFeatured);
        recyclerViewCategories = (RecyclerView) v.findViewById(R.id.recyclerViewCategories);

        linNewRelease = (LinearLayout) v.findViewById(R.id.linNewRelease);
        linFeatured = (LinearLayout) v.findViewById(R.id.linFeatured);
        linCategories = (LinearLayout) v.findViewById(R.id.linCategories);

        txtMoreNewRelease = (TextView) v.findViewById(R.id.txtMoreNewRelease);
        txtMoreFeatured = (TextView) v.findViewById(R.id.txtMoreFeatured);
        txtMoreCategories = (TextView) v.findViewById(R.id.txtMoreCategories);

        progressNewRelease = (ProgressWheel) v.findViewById(R.id.progressNewRelease);
        progressFeatured = (ProgressWheel) v.findViewById(R.id.progressFeatured);
        progressCategories = (ProgressWheel) v.findViewById(R.id.progressCategories);


        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewNewRelease.setLayoutManager(linearLayoutManager);
        recyclerViewNewRelease.setHasFixedSize(true);

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewCategories.setLayoutManager(linearLayoutManager);
        recyclerViewCategories.setHasFixedSize(true);

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewFeatured.setLayoutManager(linearLayoutManager);
        recyclerViewFeatured.setHasFixedSize(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
        adjustViewPagerPosition();
        onClickListeners();

        setVisibilityGone();

        prefs = new GetSetSharedPrefs(MainActivity.mainActivity, PreferencesKeys.PREFERENCE_SETTINGS);

        if (new ConnectionDetector(MainActivity.mainActivity).isConnectingToInternet()) {
            progressNewRelease.setVisibility(View.VISIBLE);
            progressFeatured.setVisibility(View.VISIBLE);
            progressCategories.setVisibility(View.VISIBLE);

            prefs = new GetSetSharedPrefs(MainActivity.mainActivity, PreferencesKeys.PREFERENCE_SETTINGS);
            String strPreviousResponseLatest = prefs.getData(PreferencesKeys.PREF_NEW_HOME).trim();
            String strPreviousResponseFeatured = prefs.getData(PreferencesKeys.PREF_FEATURED_HOME).trim();
            String strPreviousResponseCategory = prefs.getData(PreferencesKeys.PREF_CATEGORY_HOME).trim();
            String strPreviousResponseSlider = prefs.getData(PreferencesKeys.PREF_SLIDER_HOME).trim();

            if (strPreviousResponseLatest.trim().length() < 2)
                viewNewReleaseBooksOkHttp();
            else if (strPreviousResponseFeatured.trim().length() < 2)
                viewFeaturedBooksOkHttp();
            else if (strPreviousResponseCategory.trim().length() < 2)
                viewCatSubCatOkHttp();
            else
                offlineResponseSet();

            if (strPreviousResponseSlider.trim().length() < 2)
                okHttpViewSlider();
            else
                parseSliderImageData(strPreviousResponseSlider);

        } else {
            String strPreviousResponseSlider = prefs.getData(PreferencesKeys.PREF_SLIDER_HOME).trim();
            parseSliderImageData(strPreviousResponseSlider);
            offlineResponseSet();
            Snackbar.make(linParent, getString(R.string.connection_error), Snackbar.LENGTH_LONG).show();
        }
    }

    public void offlineResponseSet() {
        prefs = new GetSetSharedPrefs(MainActivity.mainActivity, PreferencesKeys.PREFERENCE_SETTINGS);
        String strResponseNewHome = prefs.getData(PreferencesKeys.PREF_NEW_HOME);
        String strResponseFeaturedHome = prefs.getData(PreferencesKeys.PREF_FEATURED_HOME);
        String strResponseCategories = prefs.getData(PreferencesKeys.PREF_CATEGORY_HOME);

        BookJsonParse jsonParse = new BookJsonParse(MainActivity.mainActivity);
        if (strResponseNewHome.trim().length() > 1) {
            arrayBookModel = jsonParse.parseBookResponseGetArray(strResponseNewHome);
            adapter = new BooksHomePageAdapter(MainActivity.mainActivity, arrayBookModel, "Home");
            if (active) {
                progressNewRelease.setVisibility(View.GONE);
                txtMoreNewRelease.setVisibility(View.VISIBLE);
                recyclerViewNewRelease.setAdapter(adapter);
            }
        }

        if (arrayBookModel.size() == 0)
            linNewRelease.setVisibility(View.GONE);

        jsonParse = new BookJsonParse(MainActivity.mainActivity);
        if (strResponseFeaturedHome.trim().length() > 1) {
            arrayBookModel = jsonParse.parseBookResponseGetArray(strResponseFeaturedHome);
            adapter = new BooksHomePageAdapter(MainActivity.mainActivity, arrayBookModel, "Home");
            if (active) {
                progressFeatured.setVisibility(View.GONE);
                txtMoreFeatured.setVisibility(View.VISIBLE);
                recyclerViewFeatured.setAdapter(adapter);
            }
        }

        if (arrayBookModel.size() == 0)
            linFeatured.setVisibility(View.GONE);

        if (strResponseCategories.trim().length() > 1) {
            parseCategoryAndSetAdapter(strResponseCategories);
        } else {
            linCategories.setVisibility(View.GONE);
        }

    }

    private void setVisibilityGone() {
        progressWheel.setVisibility(View.GONE);
        txtMoreNewRelease.setVisibility(View.GONE);
        txtMoreFeatured.setVisibility(View.GONE);
        txtMoreCategories.setVisibility(View.GONE);
        progressNewRelease.setVisibility(View.GONE);
        progressFeatured.setVisibility(View.GONE);
        progressCategories.setVisibility(View.GONE);
    }

    @Override
    public void onStop() {
        active = false;
        super.onStop();
    }

    private void onClickListeners() {
        txtMoreNewRelease.setOnClickListener(this);
        txtMoreFeatured.setOnClickListener(this);
        txtMoreCategories.setOnClickListener(this);
    }

    private void adjustViewPagerPosition() {
        // Disable clip to padding
        viewPager.setClipToPadding(false);
        // set padding manually, the more you set the padding the more you see of prev & next page
        viewPager.setPadding(getActivity().getResources().getDimensionPixelSize(R.dimen.dim_20), 0, getActivity().getResources().getDimensionPixelSize(R.dimen.dim_20), 0);
        // sets a margin b/w individual pages to ensure that there is a gap b/w them
        viewPager.setPageMargin(getActivity().getResources().getDimensionPixelSize(R.dimen.dim_8));
    }

    private void okHttpViewSlider() {
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_VIEW_SLIDER, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressWheel.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressWheel.setVisibility(View.GONE);
                    }
                });

                if (response.isSuccessful()) {
                    try {
                        String strResponse = Html.fromHtml(response.body().string()).toString();
                        prefs.putData(PreferencesKeys.PREF_SLIDER_HOME, strResponse);
                        parseSliderImageData(strResponse);
                    } catch (Exception e) {

                    }
                }

            }
        });
    }

    private void parseSliderImageData(String strResponse) {
        try {
            JSONObject json = new JSONObject(strResponse);
            if (json.getInt("result") == 1) {
                JSONArray array = json.getJSONArray("Message");
                arraySliderModel.clear();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    SliderModel sliderModel = new SliderModel();
                    sliderModel.setSliderId(obj.getString("slider_id"));
                    sliderModel.setSliderTitle(obj.getString("slider_title"));
                    sliderModel.setSliderAppImage(obj.getString("slider_app_image"));
                    sliderModel.setSliderFlag(obj.getString("slider_flag"));
                    sliderModel.setSliderBookOrCatId(obj.getString("slider_cat"));
                    sliderModel.setSliderCategoryName(obj.getString("category_name"));
                    arraySliderModel.add(sliderModel);
                }

                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active) {
                            sliderAdapter = new SliderAdapter(MainActivity.mainActivity, arraySliderModel);
                            viewPager.setAdapter(sliderAdapter);
                        }
                    }
                });

            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtMoreNewRelease:
                TabBarHomeFragment.viewPager.setCurrentItem(1);
                break;
            case R.id.txtMoreFeatured:
                TabBarHomeFragment.viewPager.setCurrentItem(2);
                break;
            case R.id.txtMoreCategories:
                TabBarHomeFragment.viewPager.setCurrentItem(3);
                break;
        }
    }

    public void viewNewReleaseBooksOkHttp() {

        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        KeyValueModel model = new KeyValueModel("flag", "1");
        arrayKeyValueModel.add(model);

        OkHttpCalls calls = new OkHttpCalls(Urls.API_NEW_RELEASE, arrayKeyValueModel);
        calls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active) {
                            progressNewRelease.setVisibility(View.GONE);
                            linNewRelease.setVisibility(View.GONE);
                        }
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    parseBookResponseAndSetAdapter(strResponse, 1);
                }
                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active) {
                            progressNewRelease.setVisibility(View.GONE);
                            viewFeaturedBooksOkHttp();
                        }
                    }
                });
            }
        });
    }

    public void viewFeaturedBooksOkHttp() {

        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("flag", "1")); //0 means all featured books
        arrayKeyValueModel.add(new KeyValueModel("product_quality", PreferencesKeys.PREF_FEATURED_ALL));
        OkHttpCalls calls = new OkHttpCalls(Urls.API_FEATURED, arrayKeyValueModel);
        calls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active) {
                            progressFeatured.setVisibility(View.GONE);
                            linFeatured.setVisibility(View.GONE);
                        }
                    }
                });

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    parseBookResponseAndSetAdapter(strResponse, 2);
                }
                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active)
                            progressFeatured.setVisibility(View.GONE);
                    }
                });

                viewCatSubCatOkHttp();
            }
        });
    }

    //flagAdapter=1->New Release    2->Featured
    private void parseBookResponseAndSetAdapter(String response, final int flagAdapter) {
        if (flagAdapter == 1)
            Log.v("ResponseHomeParse", "" + response);
        prefs = new GetSetSharedPrefs(MainActivity.mainActivity, PreferencesKeys.PREFERENCE_SETTINGS);
        if (flagAdapter == 1)
            prefs.putData(PreferencesKeys.PREF_NEW_HOME, response);
        else
            prefs.putData(PreferencesKeys.PREF_FEATURED_HOME, response);

        BookJsonParse jsonParse = new BookJsonParse(getActivity());
        arrayBookModel = jsonParse.parseBookResponseGetArray(response);
        MainActivity.mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (active) {
                    adapter = new BooksHomePageAdapter(getActivity(), arrayBookModel, "Home");
                    if (flagAdapter == 1) {
                        txtMoreNewRelease.setVisibility(View.VISIBLE);
                        recyclerViewNewRelease.setAdapter(adapter);
                        if (arrayBookModel.size() == 0)
                            linNewRelease.setVisibility(View.GONE);
                    } else if (flagAdapter == 2) {
                        txtMoreFeatured.setVisibility(View.VISIBLE);
                        recyclerViewFeatured.setAdapter(adapter);
                        if (arrayBookModel.size() == 0)
                            linFeatured.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    public void viewCatSubCatOkHttp() {
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        OkHttpCalls calls = new OkHttpCalls(Urls.API_CATEGORY_SUB_CATEGORY, arrayKeyValueModel);
        calls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active) {
                            progressCategories.setVisibility(View.GONE);
                            linCategories.setVisibility(View.GONE);
                        }
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (response.isSuccessful()) {
                    final String strResponse = Html.fromHtml(response.body().string()).toString();
                    prefs = new GetSetSharedPrefs(MainActivity.mainActivity, PreferencesKeys.PREFERENCE_SETTINGS);
                    prefs.putData(PreferencesKeys.PREF_CATEGORY_HOME, strResponse);
                    MainActivity.mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            parseCategoryAndSetAdapter(strResponse);
                        }
                    });
                }

                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active)
                            progressCategories.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    public void parseCategoryAndSetAdapter(String strResponse) {
        if (active) {
            txtMoreCategories.setVisibility(View.VISIBLE);
            progressCategories.setVisibility(View.GONE);
            CategorySubCategoryParse categoryParse = new CategorySubCategoryParse(MainActivity.mainActivity);
            arrayCategoryModel = categoryParse.parseCategorySubCategory(strResponse);
            categorySubCategoryAdapter = new CategorySubCategoryAdapter(MainActivity.mainActivity, arrayCategoryModel, "Home");
            recyclerViewCategories.setAdapter(categorySubCategoryAdapter);

            if (arrayCategoryModel.size() == 0)
                linCategories.setVisibility(View.GONE);
        }
    }

}
