package com.rajasthanhistory.rajasthanbooks.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.rajasthanhistory.rajasthanbooks.BookData.BookJsonParse;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.MainActivity;
import com.rajasthanhistory.rajasthanbooks.adapter.BooksHomePageAdapter;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.GetSetSharedPrefs;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;
import com.rajasthanhistory.rajasthanbooks.utils.PreferencesKeys;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by wscubetech on 4/10/16.
 */
public class FeaturedSaleFreeNewFragment extends Fragment implements View.OnClickListener {

    String comingFrom = ""; //=> New  Featured Sale Free

    ArrayList<BookModel> arrayBookModel = new ArrayList<>();
    BooksHomePageAdapter adapter;
    RecyclerView recyclerView;

    LinearLayout linMainContent, linLoading, linBooks;

    Boolean active;

    GetSetSharedPrefs prefs;

    String strResponse = "";

    //comingFrom => New  Featured
    public static Fragment newInstance(String comingFrom) {
        FeaturedSaleFreeNewFragment fragment = new FeaturedSaleFreeNewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ComingFrom", comingFrom);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_featured_new_sale_free, container, false);
        init(view);
        view.setOnClickListener(this);
        return view;
    }

    private void init(View v) {
        linMainContent = (LinearLayout) v.findViewById(R.id.linMainContent);
        linLoading = (LinearLayout) v.findViewById(R.id.linLoading);
        linBooks = (LinearLayout) v.findViewById(R.id.linBooks);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
        comingFrom = this.getArguments().getString("ComingFrom");

        prefs = new GetSetSharedPrefs(MainActivity.mainActivity, PreferencesKeys.PREFERENCE_SETTINGS);

        String url = comingFrom.equalsIgnoreCase("New") ? Urls.API_NEW_RELEASE : Urls.API_FEATURED;

        strResponse = prefs.getData(comingFrom).trim();

        if (new ConnectionDetector(getActivity()).isConnectingToInternet()) {
            if (strResponse.length() < 1)
                viewAllNewOrFeaturedOkHttp(url);
            else {
                responseSetAdapter();
            }
        }
    }

    @Override
    public void onStop() {
        active = false;
        super.onStop();
    }

    private void responseSetAdapter() {
        if (active) {
            BookJsonParse jsonParse = new BookJsonParse(getActivity());
            arrayBookModel = jsonParse.parseBookResponseGetArray(strResponse);
            adapter = new BooksHomePageAdapter(getActivity(), arrayBookModel, comingFrom);
            recyclerView.setAdapter(adapter);
            linLoading.setVisibility(View.GONE);
            linMainContent.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {

    }

    public void viewAllNewOrFeaturedOkHttp(String newFeaturedUrl) {

        linMainContent.setVisibility(View.GONE);
        linLoading.setVisibility(View.VISIBLE);

        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        if (!comingFrom.equalsIgnoreCase("New"))
            arrayKeyValueModel.add(new KeyValueModel("product_quality", comingFrom));

        OkHttpCalls calls = new OkHttpCalls(newFeaturedUrl, arrayKeyValueModel);
        calls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        linLoading.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                if (response.isSuccessful()) {
                    strResponse = Html.fromHtml(response.body().string()).toString();
                    Log.v("Response",strResponse);
                    prefs = new GetSetSharedPrefs(MainActivity.mainActivity, PreferencesKeys.PREFERENCE_SETTINGS);
                    prefs.putData(comingFrom, strResponse);
                }
                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        responseSetAdapter();
                    }
                });
            }
        });
    }

}
