package com.rajasthanhistory.rajasthanbooks.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;

/**
 * Created by wscubetech on 6/10/16.
 */
public class BookDescriptionFragment extends Fragment {

    BookModel bookModel;

    TextView txtBookDesc;

    public BookDescriptionFragment(){

    }

    public static Fragment newInstance(BookModel bookModel){
        Fragment fragment=new BookDescriptionFragment();
        Bundle bundle=new Bundle();
        bundle.putSerializable("BookModel",bookModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_book_description,container,false);
        init(view);
        return view;
    }

    private void init(View v){
        txtBookDesc=(TextView)v.findViewById(R.id.txtBookDesc);
    }

    @Override
    public void onStart() {
        super.onStart();
        bookModel=(BookModel)this.getArguments().getSerializable("BookModel");
        txtBookDesc.setText(bookModel.getBookDescription());
    }
}
