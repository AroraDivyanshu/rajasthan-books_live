package com.rajasthanhistory.rajasthanbooks.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;

/**
 * Created by wscubetech on 14/10/16.
 */
public class AuthorInfoFragment extends Fragment implements View.OnClickListener {

    TextView txtAuthor, txtAuthorInfo;
    BookModel bookModel;

    public static Fragment newInstance(BookModel bookModel) {
        Fragment fragment = new AuthorInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("BookModel", bookModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_author_info, container, false);
        view.setOnClickListener(this);
        init(view);
        return view;
    }

    private void init(View v) {
        txtAuthor = (TextView) v.findViewById(R.id.txtAuthor);
        txtAuthorInfo = (TextView) v.findViewById(R.id.txtAuthorInfo);
    }

    @Override
    public void onStart() {
        super.onStart();
        bookModel = (BookModel) this.getArguments().getSerializable("BookModel");
        txtAuthor.setText(bookModel.getBookAuthor());
        txtAuthorInfo.setText(bookModel.getBookAuthorAbout());
    }

    @Override
    public void onClick(View view) {

    }
}
