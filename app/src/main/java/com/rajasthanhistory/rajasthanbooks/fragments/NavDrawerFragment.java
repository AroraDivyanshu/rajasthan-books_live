package com.rajasthanhistory.rajasthanbooks.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.rajasthanhistory.rajasthanbooks.BookData.CategorySubCategoryParse;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.MainActivity;
import com.rajasthanhistory.rajasthanbooks.adapter.NavDrawerRecyclerViewAdapter;
import com.rajasthanhistory.rajasthanbooks.model.CategoryModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by wscubetech on 30/9/16.
 */
public class NavDrawerFragment extends Fragment {

    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private FragmentDrawerListener drawerListener;

    LinearLayout linNetworkError;
    ProgressWheel progressWheel;

    RecyclerView recyclerView;
    NavDrawerRecyclerViewAdapter drawerRecyclerViewAdapter;
    ArrayList<CategoryModel> arrayCategoryModel = new ArrayList<>();

    boolean active;

    public NavDrawerFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nav_drawer, container, false);
        init(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        active = false;
        super.onStop();
    }

    public void init(View view) {
        linNetworkError = (LinearLayout) view.findViewById(R.id.linNetworkError);
        progressWheel = (ProgressWheel) view.findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }

    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(int position);
    }

    public void setUp(DrawerLayout drawerLayout, final Toolbar toolbar) {
        active = true;
        this.drawerLayout = drawerLayout;
        drawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getActivity().supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                //getActivity().supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                //toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        drawerLayout.setDrawerListener(drawerToggle);
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                drawerToggle.syncState();
            }
        });

        if (new ConnectionDetector(getActivity()).isConnectingToInternet()) {
            viewCatSubCatOkHttp();
        } else {
            recyclerView.setVisibility(View.GONE);
            progressWheel.setVisibility(View.GONE);
            linNetworkError.setVisibility(View.VISIBLE);
        }

    }

    private void viewCatSubCatOkHttp() {
        recyclerView.setVisibility(View.GONE);
        linNetworkError.setVisibility(View.GONE);
        progressWheel.setVisibility(View.VISIBLE);

        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_CATEGORY_SUB_CATEGORY, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active) {
                            recyclerView.setVisibility(View.GONE);
                            progressWheel.setVisibility(View.GONE);
                            linNetworkError.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    CategorySubCategoryParse parse = new CategorySubCategoryParse(MainActivity.mainActivity);
                    arrayCategoryModel=parse.parseCategorySubCategory(strResponse);

                    if(active){
                        MainActivity.mainActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                drawerRecyclerViewAdapter=new NavDrawerRecyclerViewAdapter(MainActivity.mainActivity,drawerLayout,drawerListener,arrayCategoryModel);
                                recyclerView.setAdapter(drawerRecyclerViewAdapter);

                                recyclerView.setVisibility(View.VISIBLE);
                                progressWheel.setVisibility(View.GONE);
                                linNetworkError.setVisibility(View.GONE);
                            }
                        });
                    }
                }else{
                    MainActivity.mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (active) {
                                recyclerView.setVisibility(View.GONE);
                                progressWheel.setVisibility(View.GONE);
                                linNetworkError.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }
        });

    }

}
