package com.rajasthanhistory.rajasthanbooks.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.rajasthanhistory.rajasthanbooks.BookData.CategorySubCategoryParse;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.MainActivity;
import com.rajasthanhistory.rajasthanbooks.adapter.CategorySubCategoryAdapter;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.model.CategoryModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.GetSetSharedPrefs;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;
import com.rajasthanhistory.rajasthanbooks.utils.PreferencesKeys;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by wscubetech on 14/10/16.
 */
public class CategoriesFragment extends Fragment implements View.OnClickListener {

    ArrayList<CategoryModel> arrayCategoryModel = new ArrayList<>();
    CategorySubCategoryAdapter categorySubCategoryAdapter;
    RecyclerView recyclerView;
    ProgressWheel progressWheel;

    GetSetSharedPrefs prefs;
    DialogMsg dialogMsg;

    boolean active;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);
        init(view);
        view.setOnClickListener(this);
        return view;
    }

    public void init(View v) {
        progressWheel = (ProgressWheel) v.findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
        prefs = new GetSetSharedPrefs(MainActivity.mainActivity, PreferencesKeys.PREFERENCE_SETTINGS);
        String strResponse = prefs.getData(PreferencesKeys.PREF_CATEGORY_HOME);
        if (new ConnectionDetector(MainActivity.mainActivity).isConnectingToInternet()) {
            if (strResponse.trim().length() < 1)
                viewCatSubCatOkHttp();
            else
                parseCategoryAndSetAdapter(strResponse);
        }
    }

    @Override
    public void onStop() {
        active = false;
        super.onStop();
    }

    public void viewCatSubCatOkHttp() {
        progressWheel.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        OkHttpCalls calls = new OkHttpCalls(Urls.API_CATEGORY_SUB_CATEGORY, arrayKeyValueModel);
        calls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active) {
                            recyclerView.setVisibility(View.VISIBLE);
                            progressWheel.setVisibility(View.GONE);
                        }
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (response.isSuccessful()) {
                    final String strResponse = Html.fromHtml(response.body().string()).toString();
                    prefs = new GetSetSharedPrefs(MainActivity.mainActivity, PreferencesKeys.PREFERENCE_SETTINGS);
                    prefs.putData(PreferencesKeys.PREF_CATEGORY_HOME, strResponse);
                    MainActivity.mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            parseCategoryAndSetAdapter(strResponse);
                        }
                    });
                }

                MainActivity.mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active) {
                            recyclerView.setVisibility(View.VISIBLE);
                            progressWheel.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });
    }

    public void parseCategoryAndSetAdapter(String strResponse) {
        if (active) {
            recyclerView.setVisibility(View.VISIBLE);
            progressWheel.setVisibility(View.GONE);
            CategorySubCategoryParse categoryParse = new CategorySubCategoryParse(MainActivity.mainActivity);
            arrayCategoryModel = categoryParse.parseCategorySubCategory(strResponse);
            categorySubCategoryAdapter = new CategorySubCategoryAdapter(MainActivity.mainActivity, arrayCategoryModel, "Categories");
            recyclerView.setAdapter(categorySubCategoryAdapter);
        }
    }

    @Override
    public void onClick(View view) {

    }
}
