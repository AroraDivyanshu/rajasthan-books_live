package com.rajasthanhistory.rajasthanbooks.url;

/**
 * Created by wscubetech on 4/10/16.
 */
public class Urls {

    //public static final String BASE_URL = "http://www.rajasthanhistory.com/rajasthan_history/api/";
    public static final String BASE_URL = "http://www.rajasthanhistory.com/api/";

    //public static final String IMAGE_URL = "http://www.rajasthanhistory.com/rajasthan_history/";
    public static final String IMAGE_URL = "http://www.rajasthanhistory.com/";
    public static final String IMAGE_PRODUCT = IMAGE_URL + "product_image/";
    public static final String IMAGE_SLIDER = IMAGE_URL + "slider_app/";
    public static final String IMAGE_CATEGORY = IMAGE_URL + "category_image/";
    public static final String DOC_PRODUCT = IMAGE_URL + "product_files/";

    public static final String API_VIEW_SLIDER = BASE_URL + "view_slider.php";
    /*slider_flag=0,1,2,3,4
    0=>Free Book,
    1=>Featured Book,
    2=>Sale Book,
    3=>Book,
    4=>Category*/
    //slider_cat= //either category_id or product_id(book_id)

    public static final String API_NEW_RELEASE = BASE_URL + "latest_books.php";
    //flag=1 //limit 8 //else all will show

    public static final String API_FEATURED = BASE_URL + "featured_books.php?";
    //flag=1 //limit 8 //else all will show
    //product_quality= Featured   Sale   Free
    //3 in 1

    public static final String API_CATEGORY_SUB_CATEGORY = BASE_URL + "view_category.php";

    public static final String API_BOOK_DETAIL = BASE_URL + "view_books.php?";
    //product_id=6

    public static final String API_VIEW_BOOKS_CATEGORIZED = BASE_URL + "view_category_books.php?";
    //product_category=7

    public static final String API_REGISTRATION = BASE_URL + "add_register.php?";
    //first_name=&last_name=&email_id=&number=&password=

    public static final String API_LOGIN = BASE_URL + "login.php?";
    //email_id=&password=

    public static final String API_USER_DETAILS = BASE_URL + "user_detail.php?";
    //user_id=

    public static final String API_EDIT_USER = BASE_URL + "edit_user.php?";
    //user_id=&user_fname=&user_phone=

    public static final String API_VIEW_RATING_REVIEW = BASE_URL + "rating_revie.php";
    //product_id=&user_id=

    public static final String API_VIEW_USER_RATING = BASE_URL + "user_review.php?";
    //product_id=&user_id=

    public static final String API_ADD_UPDATE_RATING = BASE_URL + "add_rating_review.php?";
    //user_id=1&product_id=1&comment_rating=1&comment_message=sd

    public static final String API_ADD_REMOVE_WISH_LIST = BASE_URL + "add_wishlist.php?";
    //user_id=1&product_id=2
    //&flag=1 //send 1 to remove  //send blank to add

    public static final String API_ADD_TO_CART = BASE_URL + "add_cart.php?";
    //user_id=&book_id=

    public static final String API_VIEW_CART = BASE_URL + "view_cart.php?";
    //user_id=

    public static final String API_VIEW_WISH_LIST = BASE_URL + "view_wishlist.php?";
    //user_id=

    public static final String API_DELETE_FROM_CART = BASE_URL + "delete_cart.php?";
    //cart_id=

    public static final String API_PLACE_ORDER = BASE_URL + "place_order.php?";
    //user_id=1&total_amout=1&payment_type=1&payment_status=1&flag=1&book_detail=1
    //flag=1  //order placed from android app
    //book_detail= Book Detail Json Array
    //user_name=  //email=
    //paypal_json=

    public static final String API_VIEW_ORDER = BASE_URL + "view_order.php?";
    //user_id=1

    public static final String API_SEARCH = BASE_URL + "search_book.php?";
    //name=his

    public static final String API_VIEW_MY_BOOKS = BASE_URL + "my_books.php?";
    //user_id=7

    public static final String API_SORTING = BASE_URL + "sorting.php?";
    //flag=2
    /*flag =1 for price Asc
    flag =2 for price DESC
    flag =3 for authorname Asc
    flag =4 for book type Asc
    flag =5 for popularity Asc*/

    public static final String API_VIEW_BOOK_INDEX=BASE_URL+"pdf_index.php?";
    //book_id=8


}
