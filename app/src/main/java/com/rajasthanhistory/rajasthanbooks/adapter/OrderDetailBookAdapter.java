package com.rajasthanhistory.rajasthanbooks.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.BookDetailActivity;
import com.rajasthanhistory.rajasthanbooks.activities.ViewBookActivity;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class OrderDetailBookAdapter extends RecyclerView.Adapter<OrderDetailBookAdapter.ViewHolder> {

    ArrayList<BookModel> arrayBookModel = new ArrayList<>();
    Activity activity;
    String comingFrom = ""; //Order Detail Page or Check out page or My Books---  OrderDetail  CheckOut   MyBooks

    public OrderDetailBookAdapter(Activity activity, ArrayList<BookModel> arrayBookModel, String comingFrom) {
        this.activity = activity;
        this.arrayBookModel = arrayBookModel;
        this.comingFrom = comingFrom;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = comingFrom.equalsIgnoreCase("MyBooks") ? LayoutInflater.from(activity).inflate(R.layout.row_my_books, parent, false) : LayoutInflater.from(activity).inflate(R.layout.row_book_order_detail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final BookModel bookModel = arrayBookModel.get(position);
        holder.txtTitle.setText(bookModel.getBookTitle());
        holder.txtAuthorName.setText(bookModel.getBookAuthor());
        holder.txtDiscountedPrice.setText(activity.getString(R.string.rs) + bookModel.getBookDiscountedPrice());
        String strType = bookModel.getBookType().equals("1") ? "E-Book (" + bookModel.getBookSize() + ")" : "Hard Copy";
        holder.txtBookType.setText(strType);

        holder.imgMore.setVisibility(comingFrom.equalsIgnoreCase("MyBooks") ? View.VISIBLE : View.GONE);

        Glide.with(activity)
                .load(Urls.IMAGE_PRODUCT + bookModel.getBookImage())
                .thumbnail(0.2f)
                .override(activity.getResources().getDimensionPixelSize(R.dimen.dim_250), activity.getResources().getDimensionPixelSize(R.dimen.dim_300))
                .placeholder(R.drawable.ic_book_placeholder)
                .into(holder.imgBook);

        holder.imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.imgMore, bookModel, holder.progressDownload, holder.txtPercentage);
            }
        });

        if (comingFrom.equalsIgnoreCase("MyBooks")) {
            holder.relParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.imgMore.performClick();
                }
            });
        } else if (comingFrom.equalsIgnoreCase("OrderDetail")) {
            holder.relParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, BookDetailActivity.class);
                    intent.putExtra("BookId", bookModel.getBookId());
                    activity.startActivity(intent);
                }
            });
        }

        if (position == arrayBookModel.size() - 1)
            holder.viewLine.setVisibility(View.GONE);
    }

    private void showPopupMenu(View view, BookModel bookModel, ProgressBar progressDownload, TextView txtPercentage) {
        // inflate menu
        PopupMenu popup = new PopupMenu(activity, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_pop_up, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(bookModel, progressDownload, (ImageView) view, txtPercentage));
        popup.show();
        setForceShowIcon(popup);

        String fullPathName = giveFileCompleteFilePath(Urls.DOC_PRODUCT + bookModel.getBookUrlFileName());
        File file = new File(fullPathName);
        if (!file.exists()) {
            MenuItem item = popup.getMenu().findItem(R.id.itemView);
            item.setVisible(false);
        } else {
            MenuItem item = popup.getMenu().findItem(R.id.itemDownload);
            item.setTitle("Re-Download");
        }

        //popup.getMenu().findItem(R.id.itemDownload).setVisible(false);

    }

    public static void setForceShowIcon(PopupMenu popupMenu) {
        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
        BookModel bookModel;
        ProgressBar progressDownload;
        ImageView imgMore;
        TextView txtPercentage;

        public MyMenuItemClickListener(BookModel bookModel, ProgressBar progressDownload, ImageView imgMore, TextView txtPercentage) {
            this.bookModel = bookModel;
            this.progressDownload = progressDownload;
            this.imgMore = imgMore;
            this.txtPercentage = txtPercentage;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.itemDetails:
                    Intent intent = new Intent(activity, BookDetailActivity.class);
                    intent.putExtra("BookId", bookModel.getBookId());
                    activity.startActivity(intent);
                    return true;
                case R.id.itemView:
                    intent = new Intent(activity, ViewBookActivity.class);
                    intent.putExtra("BookModel", bookModel);
                    activity.startActivity(intent);

                    /*intent = new Intent(activity, MyPdfViewerActivity.class);
                    intent.putExtra(PdfViewerActivity.EXTRA_PDFFILENAME,
                            giveFileCompleteFilePath(Urls.DOC_PRODUCT + bookModel.getBookUrlFileName()));
                    activity.startActivity(intent);*/

                    /*String fullPathName = giveFileCompleteFilePath(Urls.DOC_PRODUCT + bookModel.getBookUrlFileName());
                    File file = new File(fullPathName);
                    if (file.exists())
                        openDocument(fullPathName);
                    else {
                        final DialogMsg dialogMsg = new DialogMsg(activity);
                        dialogMsg.showDialog("Please download the e-book first", R.drawable.ic_error_general, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogMsg.getDialog().dismiss();
                            }
                        });
                    }*/
                    return true;
                case R.id.itemDownload:
                    new DownloadFileFromURL(progressDownload, imgMore, txtPercentage).execute(Urls.DOC_PRODUCT + bookModel.getBookUrlFileName());
                    return true;
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return arrayBookModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtTitle, txtAuthorName, txtDiscountedPrice, txtBookType, txtPercentage;
        ImageView imgBook, imgMore;
        RelativeLayout relParent;
        View viewLine;
        ProgressBar progressDownload;

        public ViewHolder(View v) {
            super(v);
            relParent = (RelativeLayout) v.findViewById(R.id.relParent);
            txtTitle = (TextView) v.findViewById(R.id.txtTitle);
            txtAuthorName = (TextView) v.findViewById(R.id.txtAuthor);
            txtDiscountedPrice = (TextView) v.findViewById(R.id.txtDiscountedPrice);
            viewLine = v.findViewById(R.id.viewLine);
            txtBookType = (TextView) v.findViewById(R.id.txtBookType);
            txtPercentage = (TextView) v.findViewById(R.id.txtPercentage);
            imgBook = (ImageView) v.findViewById(R.id.imgBook);
            imgMore = (ImageView) v.findViewById(R.id.imgMore);
            progressDownload = (ProgressBar) v.findViewById(R.id.progressDownload);

            LayerDrawable layerDrawable = (LayerDrawable) progressDownload.getProgressDrawable();
            Drawable progressDrawable = layerDrawable.findDrawableByLayerId(android.R.id.progress);
            progressDrawable.setColorFilter(ContextCompat.getColor(activity, R.color.colorPrimaryDark), PorterDuff.Mode.SRC_IN);
        }
    }

    private String giveFileCompleteFilePath(String strUrl) {
        String path = Environment.getDataDirectory() + "/data/" + activity.getPackageName() + "/files/";
        String fileName = strUrl.substring(strUrl.lastIndexOf("/") + 1);
        return (path + fileName);
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        ProgressBar progressBar;
        ImageView imgMore;
        TextView txtPercentage;

        public DownloadFileFromURL(ProgressBar progressBar, ImageView imgMore, TextView txtPercentage) {
            this.progressBar = progressBar;
            this.imgMore = imgMore;
            this.txtPercentage = txtPercentage;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            txtPercentage.setVisibility(View.VISIBLE);
            imgMore.setVisibility(View.INVISIBLE);
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                String strUrl = f_url[0];
                URL url = new URL(strUrl);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lengthOfFile = connection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);


                String fullPathWithName = giveFileCompleteFilePath(strUrl);
                File thisFile = new File(fullPathWithName);
                if (!thisFile.exists())
                    thisFile.getParentFile().mkdirs();

                // Output stream to write file
                OutputStream output = new FileOutputStream(fullPathWithName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }


        protected void onProgressUpdate(String... progress) {
            progressBar.setProgress(Integer.parseInt(progress[0]));
            txtPercentage.setText(progress[0] + "%");
        }

        @Override
        protected void onPostExecute(String file_url) {
            imgMore.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            txtPercentage.setVisibility(View.GONE);
        }

    }

    public void openDocument(String wholeFilePath) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
        File file = new File(wholeFilePath);
        if (file.exists())
            Log.v("YesThere", "" + file.getAbsolutePath());


        String updatedPath = "content://" + wholeFilePath.replace("/data/data/", "");
        Uri uri = Uri.parse(updatedPath);

        intent.setDataAndType(uri, "application/msword");

        Log.v("Uri ", Uri.parse(file.getAbsolutePath()) + "");
        Log.v("PathHit", updatedPath);
        try {
            activity.startActivity(Intent.createChooser(intent, "Select an application to open:"));
        } catch (Exception e) {
            Log.v("ExceptionOpening", "" + e);
        }
    }

}
