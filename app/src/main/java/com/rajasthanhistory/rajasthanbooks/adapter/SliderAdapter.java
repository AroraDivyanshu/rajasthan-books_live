package com.rajasthanhistory.rajasthanbooks.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.makeramen.roundedimageview.RoundedImageView;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.BookDetailActivity;
import com.rajasthanhistory.rajasthanbooks.activities.BookListActivity;
import com.rajasthanhistory.rajasthanbooks.fragments.TabBarHomeFragment;
import com.rajasthanhistory.rajasthanbooks.model.CategoryModel;
import com.rajasthanhistory.rajasthanbooks.model.SliderModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;

import java.util.ArrayList;

/**
 * Created by wscubetech on 1/10/16.
 */
public class SliderAdapter extends PagerAdapter {

    ArrayList<SliderModel> arraySliderModel = new ArrayList<>();
    Activity activity;

    LayoutInflater inflater;

    public SliderAdapter(Activity activity, ArrayList<SliderModel> arraySliderModel) {
        this.activity = activity;
        this.arraySliderModel = arraySliderModel;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arraySliderModel.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = inflater.inflate(R.layout.row_banner_discount_slider, container, false);
        initAndSet(v, position);
        container.addView(v);
        return v;
    }

    private void initAndSet(View v, int position) {
        final SliderModel model = arraySliderModel.get(position);
        final RoundedImageView imgBanner = (RoundedImageView) v.findViewById(R.id.imgBanner);
        Glide.with(activity)
                .load(Urls.IMAGE_SLIDER + model.getSliderAppImage())
                .asBitmap()
                .thumbnail(0.2f)
                .placeholder(R.drawable.ic_book_placeholder)
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        imgBanner.setImageBitmap(resource);
                        return false;
                    }
                })
                .into(imgBanner);
        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //sliderFlag=0 for Free book   1 for Featured book    2 for Sale
                //sliderFlag = 3 for book detail and 4 for category page
                Intent intent = new Intent();
                switch (model.getSliderFlag()) {
                    case "0":
                        TabBarHomeFragment.viewPager.setCurrentItem(3);
                        break;
                    case "1":
                        TabBarHomeFragment.viewPager.setCurrentItem(1);
                        break;
                    case "2":
                        TabBarHomeFragment.viewPager.setCurrentItem(2);
                        break;
                    case "3":
                        intent.setClass(activity, BookDetailActivity.class);
                        intent.putExtra("BookId", model.getSliderBookOrCatId());
                        activity.startActivity(intent);
                        break;
                    case "4":
                        intent.setClass(activity, BookListActivity.class);
                        CategoryModel categoryModel = new CategoryModel();
                        categoryModel.setCategoryId(model.getSliderBookOrCatId());
                        categoryModel.setCategoryName(model.getSliderCategoryName());
                        intent.putExtra("CategorySubCategory", "Category");
                        intent.putExtra("CategoryModel", categoryModel);
                        activity.startActivity(intent);
                        break;
                }
            }
        });
    }


}
