package com.rajasthanhistory.rajasthanbooks.adapter;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.BookDetailActivity;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogRatingReview;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.model.RatingReviewModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.FailureOccurred;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by wscubetech on 7/10/16.
 */
public class RatingReviewAdapter extends RecyclerView.Adapter<RatingReviewAdapter.ViewHolder> {

    ArrayList<RatingReviewModel> arrayRatingReviewModel = new ArrayList<>();
    Activity activity;
    User user;

    DialogMsg dialogMsg, dialogWait;

    String bookId = "";

    public RatingReviewAdapter(Activity activity, ArrayList<RatingReviewModel> arrayRatingReviewModel, String bookId) {
        this.activity = activity;
        this.arrayRatingReviewModel = arrayRatingReviewModel;
        this.bookId = bookId;
        user = new UserDetails(activity).getUserDetails();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(activity).inflate(R.layout.row_rating_review, parent, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final RatingReviewModel model = arrayRatingReviewModel.get(position);
        holder.txtUserName.setText(model.getUserName());
        holder.txtDateTime.setText(model.getDateTime());
        holder.txtComment.setText(model.getComment());
        holder.txtRating.setText("" + model.getRating());

        int colorTint = model.getRating() >= 3 ? R.color.colorGreen : R.color.colorErrorRed;
        holder.imgRating.setColorFilter(ContextCompat.getColor(activity, colorTint));

        holder.imgEdit.setVisibility(model.getUserId().equals(user.getUserId()) ? View.VISIBLE : View.GONE);
        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRatingReviewDialog(model, position);
            }
        });

        if (model.getUserId().equals(user.getUserId()))
            holder.txtUserName.setTextColor(ContextCompat.getColor(activity, R.color.colorAccent));
    }

    private void showRatingReviewDialog(RatingReviewModel ratingReviewModel, final int position) {
        final DialogRatingReview dialogRatingReview = new DialogRatingReview(BookDetailActivity.bookDetailActivity);
        dialogRatingReview.showRatingReviewDialog(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogRatingReview.getRating() == 0) {
                    Toast.makeText(BookDetailActivity.bookDetailActivity, "Please provide rating to submit", Toast.LENGTH_LONG).show();
                    return;
                }
                dialogRatingReview.getDialog().dismiss();
                if (!new ConnectionDetector(BookDetailActivity.bookDetailActivity).isConnectingToInternet()) {
                    dialogMsg = new DialogMsg(BookDetailActivity.bookDetailActivity);
                    dialogMsg.showDialog(activity.getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogMsg.getDialog().dismiss();
                        }
                    });
                    return;
                }

                okHttpAddUpdateRating(dialogRatingReview.getRating(), dialogRatingReview.getReview(), position);

            }
        }, ratingReviewModel.getRating(), ratingReviewModel.getComment());

    }

    private void okHttpAddUpdateRating(final float rating, final String msg, final int position) {

        dialogWait = new DialogMsg(BookDetailActivity.bookDetailActivity);
        dialogWait.showPleaseWaitDialog();


        final ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        arrayKeyValueModel.add(new KeyValueModel("product_id", bookId));
        arrayKeyValueModel.add(new KeyValueModel("comment_rating", rating + ""));
        arrayKeyValueModel.add(new KeyValueModel("comment_message", msg));
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_ADD_UPDATE_RATING, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                BookDetailActivity.bookDetailActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogWait.getDialog().dismiss();
                    }
                });
                FailureOccurred failureOccurred = new FailureOccurred(BookDetailActivity.bookDetailActivity);
                failureOccurred.showFailureDialog(true);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    Log.v("Response", strResponse);
                    try {
                        JSONObject jsonObject = new JSONObject(strResponse);
                        if (jsonObject.getInt("result") == 1) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    RatingReviewModel model = arrayRatingReviewModel.get(position);
                                    model.setRating(rating);
                                    model.setComment(msg);
                                    model.setDateTime("just now");
                                    arrayRatingReviewModel.set(position, model);
                                    notifyDataSetChanged();

                                    dialogMsg = new DialogMsg(activity);
                                    dialogMsg.showDialog("Rating successfully submitted", R.drawable.ic_success, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialogMsg.getDialog().dismiss();

                                        }
                                    });

                                }
                            });

                        }
                    } catch (Exception e) {
                        Log.v("Exception", "" + e);
                    }
                } else {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            dialogMsg = new DialogMsg(BookDetailActivity.bookDetailActivity);
                            dialogMsg.showDialog(activity.getString(R.string.network_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogMsg.getDialog().dismiss();
                                }
                            });


                        }
                    });
                }

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogWait.getDialog().dismiss();
                    }
                });

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayRatingReviewModel.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtUserName, txtComment, txtDateTime;
        ImageView imgRating, imgEdit;
        TextView txtRating;

        public ViewHolder(View v) {
            super(v);
            txtUserName = (TextView) v.findViewById(R.id.txtUserName);
            txtComment = (TextView) v.findViewById(R.id.txtComment);
            txtDateTime = (TextView) v.findViewById(R.id.txtDateTime);
            imgRating = (ImageView) v.findViewById(R.id.imgRating);
            txtRating = (TextView) v.findViewById(R.id.txtRating);
            imgEdit = (ImageView) v.findViewById(R.id.imgEdit);
        }
    }
}
