package com.rajasthanhistory.rajasthanbooks.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.url.Urls;

import java.util.ArrayList;

/**
 * Created by wscubetech on 5/10/16.
 */
public class BookDetailSliderAdapter extends PagerAdapter {

    ArrayList<String> arrayBookImageModel=new ArrayList<>();
    Activity activity;
    LayoutInflater inflater;

    public BookDetailSliderAdapter(Activity activity,ArrayList<String> arrayBookImageModel){
        this.activity=activity;
        this.arrayBookImageModel=arrayBookImageModel;
        inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrayBookImageModel.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = inflater.inflate(R.layout.row_book_image_pager, container, false);
        initAndSet(v, position);
        container.addView(v);
        return v;
    }

    private void initAndSet(View v, int position) {
        String strBookImage=arrayBookImageModel.get(position);
        ImageView imgBook=(ImageView)v.findViewById(R.id.imgBook);
        Glide.with(activity)
                .load(Urls.IMAGE_PRODUCT + strBookImage)
                .thumbnail(0.2f)
                .override(activity.getResources().getDimensionPixelSize(R.dimen.dim_300), activity.getResources().getDimensionPixelSize(R.dimen.dim_420))
                .placeholder(R.drawable.ic_book_placeholder)
                .into(imgBook);
    }
}
