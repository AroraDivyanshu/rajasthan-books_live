package com.rajasthanhistory.rajasthanbooks.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.BookListActivity;
import com.rajasthanhistory.rajasthanbooks.fragments.NavDrawerFragment;
import com.rajasthanhistory.rajasthanbooks.model.CategoryModel;
import com.rajasthanhistory.rajasthanbooks.model.SubCategoryModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wscubetech on 30/9/16.
 */
public class NavDrawerRecyclerViewAdapter extends ExpandableRecyclerAdapter<NavDrawerRecyclerViewAdapter.CategoryViewHolder, NavDrawerRecyclerViewAdapter.SubCategoryViewHolder> {

    private LayoutInflater inflater;
    Activity activity;
    NavDrawerFragment.FragmentDrawerListener drawerListener;
    DrawerLayout drawerLayout;


    ArrayList<CategoryModel> arrayCategoryModel = new ArrayList<>();
    ArrayList<SubCategoryModel> arraySubCategoryModel = new ArrayList<>();

    public NavDrawerRecyclerViewAdapter(Activity activity, DrawerLayout drawerLayout, NavDrawerFragment.FragmentDrawerListener drawerListener, @NonNull List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        this.activity = activity;
        this.drawerListener = drawerListener;
        this.drawerLayout = drawerLayout;
        inflater = LayoutInflater.from(activity);
    }

    @Override
    public CategoryViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View view = inflater.inflate(R.layout.row_category, parentViewGroup, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public SubCategoryViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View view = inflater.inflate(R.layout.row_sub_category, childViewGroup, false);
        return new SubCategoryViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(CategoryViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        CategoryModel categoryModel = (CategoryModel) parentListItem;
        parentViewHolder.bind(categoryModel, position);
    }

    @Override
    public void onBindChildViewHolder(SubCategoryViewHolder childViewHolder, int position, Object childListItem) {
        SubCategoryModel subCategoryModel = (SubCategoryModel) childListItem;
        childViewHolder.bind(subCategoryModel, position);
    }

    public class CategoryViewHolder extends ParentViewHolder {

        TextView txtCategoryName;
        LinearLayout linCategory;

        public CategoryViewHolder(View v) {
            super(v);
            txtCategoryName = (TextView) v.findViewById(R.id.txtCategoryName);
            linCategory = (LinearLayout) v.findViewById(R.id.linCategory);
        }

        public void bind(final CategoryModel categoryModel, final int position) {
            txtCategoryName.setText(categoryModel.getCategoryName());
            if (categoryModel.getArraySubCategoryModel().size() == 0) {
                linCategory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        drawerListener.onDrawerItemSelected(position);
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        Intent intent = new Intent(activity, BookListActivity.class);
                        intent.putExtra("CategoryModel", categoryModel);
                        intent.putExtra("CategorySubCategory", "Category");
                        activity.startActivity(intent);
                    }
                });
            }
        }
    }

    public class SubCategoryViewHolder extends ChildViewHolder {
        TextView txtSubCategoryName;
        LinearLayout linSubCategory;

        public SubCategoryViewHolder(View v) {
            super(v);
            txtSubCategoryName = (TextView) v.findViewById(R.id.txtSubCategoryName);
            linSubCategory = (LinearLayout) v.findViewById(R.id.linSubCategory);
        }


        public void bind(final SubCategoryModel subCategoryModel, final int position) {
            txtSubCategoryName.setText(subCategoryModel.getSubCategoryName());
            linSubCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    drawerListener.onDrawerItemSelected(position);
                    drawerLayout.closeDrawer(Gravity.LEFT);
                    Intent intent = new Intent(activity, BookListActivity.class);
                    intent.putExtra("SubCategoryModel", subCategoryModel);
                    intent.putExtra("CategorySubCategory", "SubCategory");
                    activity.startActivity(intent);
                }
            });
        }
    }

}
