package com.rajasthanhistory.rajasthanbooks.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.BookDetailActivity;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.utils.PreferencesKeys;

import java.util.ArrayList;

/**
 * Created by wscubetech on 1/10/16.
 */
public class BooksHomePageAdapter extends RecyclerView.Adapter<BooksHomePageAdapter.ViewHolder> {

    ArrayList<BookModel> arrayBookModel = new ArrayList<>();
    Activity activity;
    String comingFrom; //Home //New //Featured

    public BooksHomePageAdapter(Activity activity, ArrayList<BookModel> arrayBookModel, String comingFrom) {
        this.activity = activity;
        this.arrayBookModel = arrayBookModel;
        this.comingFrom = comingFrom;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = comingFrom.equalsIgnoreCase("Home") ? R.layout.row_item_book : R.layout.row_book_item_grid;
        View view = LayoutInflater.from(activity).inflate(layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final BookModel bookModel = arrayBookModel.get(position);
        holder.txtTitle.setText(bookModel.getBookTitle());
        holder.txtPrice.setText(activity.getString(R.string.rs) + "" + bookModel.getBookPrice());
        holder.txtDiscountedPrice.setText(activity.getString(R.string.rs) + "" + bookModel.getBookDiscountedPrice());

        Glide.with(activity)
                .load(Urls.IMAGE_PRODUCT + bookModel.getBookImage())
                .thumbnail(0.2f)
                .override(activity.getResources().getDimensionPixelSize(R.dimen.dim_300), activity.getResources().getDimensionPixelSize(R.dimen.dim_420))
                .placeholder(R.drawable.ic_book_placeholder)
                .into(holder.imgBook);

        if (bookModel.getBookDiscountedPrice().equals(bookModel.getBookPrice())) {
            holder.txtDiscountedPrice.setVisibility(View.VISIBLE);
            holder.txtPrice.setVisibility(View.GONE);
        } else {
            holder.txtDiscountedPrice.setVisibility(View.VISIBLE);
            holder.txtPrice.setVisibility(View.VISIBLE);
            holder.txtPrice.setPaintFlags(holder.txtPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        if (bookModel.getBookPrice().equals("0"))
            holder.txtPrice.setText(PreferencesKeys.PREF_FREE_ALL);

        holder.relBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, BookDetailActivity.class);
                intent.putExtra("BookId", bookModel.getBookId());
                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayBookModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgBook;
        TextView txtTitle, txtPrice, txtDiscountedPrice;
        RelativeLayout relBook;

        public ViewHolder(View v) {
            super(v);
            relBook = (RelativeLayout) v.findViewById(R.id.relBook);
            imgBook = (ImageView) v.findViewById(R.id.imgBook);
            txtTitle = (TextView) v.findViewById(R.id.txtTitle);
            txtPrice = (TextView) v.findViewById(R.id.txtPrice);
            txtDiscountedPrice = (TextView) v.findViewById(R.id.txtDiscountedPrice);
        }
    }

}
