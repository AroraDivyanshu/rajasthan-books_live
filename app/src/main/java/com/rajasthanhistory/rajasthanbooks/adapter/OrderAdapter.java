package com.rajasthanhistory.rajasthanbooks.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.OrderDetailActivity;
import com.rajasthanhistory.rajasthanbooks.model.OrderModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;

import java.util.ArrayList;

/**
 * Created by wscubetech on 19/10/16.
 */
public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    ArrayList<OrderModel> arrayOrderModel = new ArrayList<>();
    Activity activity;

    public OrderAdapter(Activity activity, ArrayList<OrderModel> arrayOrderModel) {
        this.activity = activity;
        this.arrayOrderModel = arrayOrderModel;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.row_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final OrderModel model = arrayOrderModel.get(position);
        holder.txtOrderNameCode.setText(model.getOrderCodeName());
        holder.txtTotalAmount.setText(activity.getString(R.string.rs) + model.getOrderTotalAmount());
        holder.txtDate.setText(model.getOrderDate());
        if (model.getArrayBookModel().size() > 0) {
            holder.txtBookTitle.setText(model.getArrayBookModel().get(0).getBookTitle());
            Glide.with(activity)
                    .load(Urls.IMAGE_PRODUCT + model.getArrayBookModel().get(0).getBookImage())
                    .thumbnail(0.2f)
                    .override(activity.getResources().getDimensionPixelSize(R.dimen.dim_250), activity.getResources().getDimensionPixelSize(R.dimen.dim_300))
                    .placeholder(R.drawable.ic_book_placeholder)
                    .into(holder.imgBook);
        }

        holder.relParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, OrderDetailActivity.class);
                intent.putExtra("OrderModel", model);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayOrderModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtOrderNameCode, txtBookTitle, txtTotalAmount,txtDate;
        ImageView imgBook;
        RelativeLayout relParent;

        public ViewHolder(View v) {
            super(v);
            txtBookTitle = (TextView) v.findViewById(R.id.txtBookTitle);
            txtOrderNameCode = (TextView) v.findViewById(R.id.txtOrderNameCode);
            txtTotalAmount = (TextView) v.findViewById(R.id.txtTotalAmount);
            txtDate=(TextView)v.findViewById(R.id.txtDate);
            imgBook = (ImageView) v.findViewById(R.id.imgBook);
            relParent = (RelativeLayout) v.findViewById(R.id.relParent);
        }
    }
}
