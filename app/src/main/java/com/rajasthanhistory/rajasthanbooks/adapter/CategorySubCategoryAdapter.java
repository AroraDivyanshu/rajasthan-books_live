package com.rajasthanhistory.rajasthanbooks.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.BookListActivity;
import com.rajasthanhistory.rajasthanbooks.model.CategoryModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;

import java.util.ArrayList;

/**
 * Created by wscubetech on 5/10/16.
 */
public class CategorySubCategoryAdapter extends RecyclerView.Adapter<CategorySubCategoryAdapter.ViewHolder> {

    Activity activity;
    ArrayList<CategoryModel> arrayCategoryModel = new ArrayList<>();
    String comingFrom = "";

    public CategorySubCategoryAdapter(Activity activity, ArrayList<CategoryModel> arrayCategoryModel, String comingFrom) {
        this.activity = activity;
        this.arrayCategoryModel = arrayCategoryModel;
        this.comingFrom = comingFrom;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = comingFrom.equalsIgnoreCase("Home") ? R.layout.row_category_sub_category : R.layout.row_book_item_grid;
        View view = LayoutInflater.from(activity).inflate(layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CategoryModel categoryModel = arrayCategoryModel.get(position);
        holder.txtTitle.setText(categoryModel.getCategoryName());
        Glide.with(activity)
                .load(Urls.IMAGE_CATEGORY + categoryModel.getCategoryPhoto())
                .thumbnail(0.2f)
                .override(activity.getResources().getDimensionPixelSize(R.dimen.dim_300), activity.getResources().getDimensionPixelSize(R.dimen.dim_420))
                .placeholder(R.drawable.ic_book_placeholder)
                .into(holder.imgBook);

        if (comingFrom.equalsIgnoreCase("Categories")) {
            holder.txtPrice.setVisibility(View.GONE);
            holder.txtDiscountedPrice.setVisibility(View.GONE);
        }

        holder.relBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, BookListActivity.class);
                intent.putExtra("CategoryModel", categoryModel);
                intent.putExtra("CategorySubCategory", "Category");
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayCategoryModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgBook;
        TextView txtTitle, txtPrice, txtDiscountedPrice;
        RelativeLayout relBook;

        public ViewHolder(View v) {
            super(v);
            relBook = (RelativeLayout) v.findViewById(R.id.relBook);
            imgBook = (ImageView) v.findViewById(R.id.imgBook);
            txtTitle = (TextView) v.findViewById(R.id.txtTitle);
            if (comingFrom.equalsIgnoreCase("Categories")) {
                txtPrice = (TextView) v.findViewById(R.id.txtPrice);
                txtDiscountedPrice = (TextView) v.findViewById(R.id.txtDiscountedPrice);
            }
        }
    }
}
