package com.rajasthanhistory.rajasthanbooks.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rajasthanhistory.rajasthanbooks.BookData.BookWishListOperation;
import com.rajasthanhistory.rajasthanbooks.BookData.CartBadgeCounter;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.BookDetailActivity;
import com.rajasthanhistory.rajasthanbooks.activities.CartWishListActivity;
import com.rajasthanhistory.rajasthanbooks.activities.MyOrdersActivity;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.DownloadFile;
import com.rajasthanhistory.rajasthanbooks.utils.FailureOccurred;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by wscubetech on 14/10/16.
 */
public class BookCartWishListAdapter extends RecyclerView.Adapter<BookCartWishListAdapter.ViewHolder> {

    ArrayList<BookModel> arrayBookModel = new ArrayList<>();
    Activity activity;
    boolean active;
    String comingFrom = ""; //Cart //Wish List

    DialogMsg dialogMsg;

    public BookCartWishListAdapter(Activity activity, ArrayList<BookModel> arrayBookModel, String comingFrom, boolean active) {
        this.activity = activity;
        this.arrayBookModel = arrayBookModel;
        this.comingFrom = comingFrom;
        this.active = active;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder = new ViewHolder(LayoutInflater.from(activity).inflate(R.layout.row_wish_list_cart, parent, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final BookModel bookModel = arrayBookModel.get(position);
        holder.txtTitle.setText(bookModel.getBookTitle());
        holder.txtAuthor.setText("BY " + bookModel.getBookAuthor().toUpperCase().trim());
        holder.txtPrice.setText(activity.getString(R.string.rs) + "" + bookModel.getBookPrice());
        holder.txtDiscountedPrice.setText(activity.getString(R.string.rs) + "" + bookModel.getBookDiscountedPrice());

        Glide.with(activity)
                .load(Urls.IMAGE_PRODUCT + bookModel.getBookImage())
                .thumbnail(0.2f)
                .override(activity.getResources().getDimensionPixelSize(R.dimen.dim_250), activity.getResources().getDimensionPixelSize(R.dimen.dim_300))
                .placeholder(R.drawable.ic_book_placeholder)
                .into(holder.imgBook);

        /*DownloadFile downloadFile = new DownloadFile(activity);
        downloadFile.startDownloading(Urls.IMAGE_PRODUCT + bookModel.getBookImage());*/

        if (bookModel.getBookDiscountedPrice().equals(bookModel.getBookPrice())) {
            holder.txtDiscountedPrice.setVisibility(View.VISIBLE);
            holder.txtPrice.setVisibility(View.GONE);
        } else {
            holder.txtDiscountedPrice.setVisibility(View.VISIBLE);
            holder.txtPrice.setVisibility(View.VISIBLE);
            holder.txtPrice.setPaintFlags(holder.txtPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        String strType = bookModel.getBookType().equals("1") ? "E-Book ("+bookModel.getBookSize()+")" : "Hard Copy";
        holder.txtBookType.setText(strType);

        holder.txtDelete.setVisibility(comingFrom.equals(activity.getString(R.string.title_cart)) ? View.VISIBLE : View.GONE);
        holder.imgFavorite.setVisibility(comingFrom.equals(activity.getString(R.string.title_wish_list)) ? View.VISIBLE : View.GONE);
        holder.imgFavorite.setImageResource(bookModel.isWished() ? R.drawable.ic_favorite_white_24dp : R.drawable.ic_favorite_border_white_24dp);
        holder.txtCategoryName.setVisibility(comingFrom.equalsIgnoreCase("Search") ? View.VISIBLE : View.GONE);

        if (comingFrom.equalsIgnoreCase(activity.getString(R.string.title_wish_list))) {
            holder.imgFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (new ConnectionDetector(activity).isConnectingToInternet()) {
                        bookModel.setWished(!bookModel.isWished());
                        holder.imgFavorite.setImageResource(bookModel.isWished() ? R.drawable.ic_favorite_white_24dp : R.drawable.ic_favorite_border_white_24dp);
                        if (bookModel.isWished()) {
                            BookWishListOperation operation = new BookWishListOperation(activity);
                            operation.addRemoveFavoriteOkHttp("", active, bookModel, holder.imgFavorite);
                        } else {
                            BookWishListOperation operation = new BookWishListOperation(activity);
                            operation.addRemoveFavoriteOkHttp("1", active, bookModel, holder.imgFavorite);
                        }
                    } else {
                        final DialogMsg dialogMsg = new DialogMsg(activity);
                        dialogMsg.showDialog(activity.getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogMsg.getDialog().dismiss();
                            }
                        });
                    }
                }
            });

        }

        if (comingFrom.equalsIgnoreCase(activity.getString(R.string.title_cart))) {
            holder.txtDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (new ConnectionDetector(activity).isConnectingToInternet()) {
                        okHttpDeleteFromCart(position);
                    } else {
                        dialogMsg = new DialogMsg(activity);
                        dialogMsg.showDialog(activity.getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogMsg.getDialog().dismiss();
                            }
                        });
                    }
                }
            });
        }

        if (comingFrom.equalsIgnoreCase("BookList") || comingFrom.equalsIgnoreCase("Search")) {
            holder.relBook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, BookDetailActivity.class);
                    intent.putExtra("BookId", bookModel.getBookId());
                    activity.startActivity(intent);
                }
            });

            if (comingFrom.equalsIgnoreCase("Search"))
                holder.txtCategoryName.setText(bookModel.getCategoryName());

        }

    }

    @Override
    public int getItemCount() {
        return arrayBookModel.size();
    }

    private void okHttpDeleteFromCart(final int position) {
        User user = new UserDetails(activity).getUserDetails();

        BookModel bookModel = arrayBookModel.get(position);
        dialogMsg = new DialogMsg(activity);
        dialogMsg.showPleaseWaitDialog();

        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("cart_id", bookModel.getCartId()));
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_DELETE_FROM_CART, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogMsg.getDialog().dismiss();
                    }
                });
                FailureOccurred occurred = new FailureOccurred(activity);
                occurred.showFailureDialog(false);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogMsg.getDialog().dismiss();
                    }
                });
                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    try {
                        final JSONObject json = new JSONObject(strResponse);

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (json.getInt("result") == 1) {
                                        CartWishListActivity.total = 0;

                                        if (arrayBookModel.size() > 1) {
                                            arrayBookModel.remove(position);
                                            for (BookModel bookModel : arrayBookModel)
                                                CartWishListActivity.total += Float.parseFloat(bookModel.getBookDiscountedPrice());
                                        } else
                                            arrayBookModel.clear();

                                        CartWishListActivity.txtTotalAmount.setText(activity.getString(R.string.rs) + CartWishListActivity.total);


                                        notifyDataSetChanged();

                                        if (arrayBookModel.size() == 0)
                                            activity.finish();
                                    } else {
                                        dialogMsg = new DialogMsg(activity);
                                        dialogMsg.showDialog(activity.getString(R.string.network_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                dialogMsg.getDialog().dismiss();
                                            }
                                        });
                                    }
                                } catch (Exception e) {

                                }
                            }
                        });
                    } catch (Exception e) {

                    }
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtTitle, txtAuthor, txtBookType, txtPrice, txtDiscountedPrice, txtDelete, txtCategoryName;
        ImageView imgBook, imgFavorite;
        RelativeLayout relBook;

        public ViewHolder(View v) {
            super(v);
            imgFavorite = (ImageView) v.findViewById(R.id.imgFavorite);
            txtTitle = (TextView) v.findViewById(R.id.txtTitle);
            txtAuthor = (TextView) v.findViewById(R.id.txtAuthor);
            txtCategoryName = (TextView) v.findViewById(R.id.txtCategoryName);
            txtPrice = (TextView) v.findViewById(R.id.txtPrice);
            txtBookType = (TextView) v.findViewById(R.id.txtBookType);
            txtDiscountedPrice = (TextView) v.findViewById(R.id.txtDiscountedPrice);
            txtDelete = (TextView) v.findViewById(R.id.txtDelete);
            imgBook = (ImageView) v.findViewById(R.id.imgBook);
            relBook = (RelativeLayout) v.findViewById(R.id.relBook);
        }
    }
}
