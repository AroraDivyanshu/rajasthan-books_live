package com.rajasthanhistory.rajasthanbooks.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.rajasthanhistory.rajasthanbooks.R;

/**
 * Created by wscubetech on 21/10/16.
 */
public class DialogRatingReview {

    Activity activity;
    Dialog dialog;
    float ratingStar = 0;
    String review = "";
    DialogRatingReview dialogRatingReview;

    public DialogRatingReview(Activity activity) {
        this.activity = activity;
        this.dialog = new MyDialog(activity).getMyDialog(R.layout.dialog_add_update_rating_review);
        dialogRatingReview = this;
    }

    public void showRatingReviewDialog(View.OnClickListener onClickListener, float rating, String review) {
        this.ratingStar = rating;
        this.review = review;

        RatingBar ratingBar;
        TextInputLayout inpReview;
        EditText etReview;
        TextView txtSubmit;
        ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
        inpReview = (TextInputLayout) dialog.findViewById(R.id.inpReview);
        etReview = (EditText) dialog.findViewById(R.id.etReview);
        txtSubmit = (TextView) dialog.findViewById(R.id.txtSubmit);

        ratingBar.setRating(ratingStar);
        etReview.setText(review);
        etReview.setSelection(etReview.getText().toString().trim().length());

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                ratingStar = v;
            }
        });

        etReview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                dialogRatingReview.review = editable.toString().trim();
            }
        });

        txtSubmit.setOnClickListener(onClickListener);
        dialog.show();
    }

    public float getRating() {
        return ratingStar;
    }

    public String getReview() {
        return review;
    }

    public Dialog getDialog() {
        return dialog;
    }

}
