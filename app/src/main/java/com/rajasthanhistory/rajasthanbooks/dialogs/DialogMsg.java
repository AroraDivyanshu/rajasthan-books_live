package com.rajasthanhistory.rajasthanbooks.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rajasthanhistory.rajasthanbooks.R;

/**
 * Created by wscubetech on 13/9/16.
 */
public class DialogMsg {
    Activity activity;
    Dialog dialog;

    public View.OnClickListener dismissClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getDialog().dismiss();
        }
    };

    public DialogMsg(Activity activity) {
        this.activity = activity;
    }

    public void showPleaseWaitDialog() {
        dialog = new MyDialog(activity).getMyDialog(R.layout.dialog_please_wait);
        dialog.setCancelable(false);
        dialog.show();
    }


    public void showDialog(String msg, int drawableRes, View.OnClickListener onClickListener) {
        dialog = new MyDialog(activity).getMyDialog(R.layout.dialog_msg_error_success);
        dialog.setCancelable(false);
        LinearLayout linParent;
        ImageView img;
        linParent = (LinearLayout) dialog.findViewById(R.id.linParent);
        img = (ImageView) dialog.findViewById(R.id.img);
        TextView txtOk = (TextView) dialog.findViewById(R.id.txtOk);
        TextView txtMsg = (TextView) dialog.findViewById(R.id.txtMsg);
        txtMsg.setText(msg);
        img.setImageResource(drawableRes);
        txtOk.setOnClickListener(onClickListener);

        linParent.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.slide_up));
        dialog.show();
    }

    public Dialog getDialog() {
        return dialog;
    }
}
