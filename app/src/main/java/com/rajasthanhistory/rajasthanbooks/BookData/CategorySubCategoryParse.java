package com.rajasthanhistory.rajasthanbooks.BookData;

import android.app.Activity;
import android.util.Log;

import com.rajasthanhistory.rajasthanbooks.model.CategoryModel;
import com.rajasthanhistory.rajasthanbooks.model.SubCategoryModel;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.GetSetSharedPrefs;
import com.rajasthanhistory.rajasthanbooks.utils.PreferencesKeys;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by wscubetech on 5/10/16.
 */
public class CategorySubCategoryParse {
    Activity activity;
    ArrayList<CategoryModel> arrayCategoryModel = new ArrayList<>();
    GetSetSharedPrefs prefs;

    public CategorySubCategoryParse(Activity activity) {
        this.activity = activity;
        prefs = new GetSetSharedPrefs(activity, PreferencesKeys.PREFERENCE_SETTINGS);
    }

    public ArrayList<CategoryModel> parseCategorySubCategory(String strResponse) {
        arrayCategoryModel = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(strResponse);
            JSONArray array = json.getJSONArray("Message");
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                CategoryModel categoryModel = new CategoryModel();
                categoryModel.setCategoryId(obj.getString("category_id"));
                categoryModel.setCategoryName(obj.getString("category_name"));
                categoryModel.setCategoryPhoto(obj.getString("category_popimage"));

                JSONArray subArray = obj.getJSONArray("sub_category");
                ArrayList<SubCategoryModel> arraySubCategoryModel = new ArrayList<>();
                for (int j = 0; j < subArray.length(); j++) {
                    JSONObject subObj = subArray.getJSONObject(j);
                    SubCategoryModel subCategoryModel = new SubCategoryModel();
                    subCategoryModel.setSubcategoryId(subObj.getString("category_id"));
                    subCategoryModel.setSubCategoryName(subObj.getString("category_name"));
                    subCategoryModel.setSubCategoryPhoto(subObj.getString("category_popimage"));
                    arraySubCategoryModel.add(subCategoryModel);
                }
                categoryModel.setArraySubCategoryModel(arraySubCategoryModel);
                arrayCategoryModel.add(categoryModel);
            }
        } catch (Exception e) {
            Log.v("CatException", "" + e);
        }

        return arrayCategoryModel;
    }


}
