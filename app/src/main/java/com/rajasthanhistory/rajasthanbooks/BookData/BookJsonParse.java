package com.rajasthanhistory.rajasthanbooks.BookData;

import android.app.Activity;
import android.text.Html;
import android.util.Log;
import android.view.View;

import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.adapter.BooksHomePageAdapter;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.Response;

/**
 * Created by wscubetech on 5/10/16.
 */
public class BookJsonParse {

    Activity activity;
    ArrayList<BookModel> arrayBookModel = new ArrayList<>();
    String comingFrom = "";

    public BookJsonParse(Activity activity) {
        this.activity = activity;
    }

    //comingFrom  //Cart //Wish List
    public BookJsonParse(Activity activity, String comingFrom) {
        this(activity);
        this.comingFrom = comingFrom;
    }

    public ArrayList<BookModel> parseBookResponseGetArray(String response) {
        try {

            JSONObject json = new JSONObject(response);
            if (json.getInt("result") == 1) {
                JSONArray array = json.getJSONArray("Message");
                arrayBookModel.clear();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    BookModel model = new BookModel();
                    model.setBookId(obj.getString("product_id"));
                    model.setBookTitle(obj.getString("product_name"));
                    model.setBookPrice(obj.getString("product_price"));
                    model.setBookDiscountedPrice(obj.getString("product_dis_price"));

                    int discountedPrice = Integer.parseInt(model.getBookPrice()) - Integer.parseInt(model.getBookDiscountedPrice());
                    model.setBookDiscountedPrice("" + discountedPrice);

                    model.setBookAuthor(obj.getString("author"));
                    model.setBookType(obj.getString("book_type"));
                    model.setBookUrlFileName(obj.getString("upload_ebook"));
                    model.setBookImage(obj.getString("product_popimage"));
                    try {
                        if (comingFrom.trim().length() > 0) {

                            model.setBookSize(obj.getString("product_length"));

                            if (comingFrom.equalsIgnoreCase(activity.getString(R.string.title_cart)))
                                model.setCartId(obj.getString("cart_id"));

                            if (comingFrom.equalsIgnoreCase(activity.getString(R.string.title_wish_list)))
                                model.setWished(true);
                        }
                    } catch (Exception e) {

                    }

                    arrayBookModel.add(model);
                }

            }

        } catch (Exception e) {
            Log.v("Exception", "" + e);
        }

        return arrayBookModel;
    }

}
