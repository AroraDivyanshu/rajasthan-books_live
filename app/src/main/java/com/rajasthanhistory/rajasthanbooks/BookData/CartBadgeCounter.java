package com.rajasthanhistory.rajasthanbooks.BookData;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.activities.BookDetailActivity;
import com.rajasthanhistory.rajasthanbooks.activities.CartWishListActivity;

/**
 * Created by wscubetech on 18/10/16.
 */
public class CartBadgeCounter {

    Activity activity;
    RelativeLayout relBadge;
    TextView txtCartCounter;
    MenuItem menuItem;

    public CartBadgeCounter(Activity activity, MenuItem menuItem) {
        this.activity = activity;
        this.menuItem = menuItem;
    }

    public void initiateView() {
        MenuItemCompat.setActionView(menuItem, R.layout.content_text_view_counter_badge);
        relBadge = (RelativeLayout) MenuItemCompat.getActionView(menuItem);
        txtCartCounter = (TextView) relBadge.findViewById(R.id.txtCartCounter);
        txtCartCounter.setVisibility(View.GONE);
        relBadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentCartWishList = new Intent(activity, CartWishListActivity.class);
                intentCartWishList.putExtra("ComingFrom", activity.getString(R.string.title_cart));
                activity.startActivity(intentCartWishList);
            }
        });
    }

    public void setBadgeCount(int count) {
        if (count > 0) {
            txtCartCounter.setVisibility(View.VISIBLE);
            txtCartCounter.setText("" + count);
            if (activity instanceof BookDetailActivity)
                relBadge.startAnimation(AnimationUtils.loadAnimation(activity, R.anim.zoom_in_overshoot));
        } else {
            txtCartCounter.setVisibility(View.GONE);
        }
    }

}
