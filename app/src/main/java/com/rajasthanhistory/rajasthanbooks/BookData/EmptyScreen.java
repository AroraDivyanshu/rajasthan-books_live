package com.rajasthanhistory.rajasthanbooks.BookData;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rajasthanhistory.rajasthanbooks.R;

/**
 * Created by wscubetech on 18/10/16.
 */
public class EmptyScreen {

    Activity activity;
    TextView txtEmptyMsg;
    ImageView imgEmptyIcon;
    LinearLayout linEmpty;

    public EmptyScreen(Activity activity) {
        this.activity = activity;
        txtEmptyMsg = (TextView) activity.findViewById(R.id.txtEmptyMsg);
        imgEmptyIcon = (ImageView) activity.findViewById(R.id.imgEmptyIcon);
        linEmpty = (LinearLayout) activity.findViewById(R.id.linEmpty);
    }

    public void setIconAndMsg(String msg, int imgRes) {
        linEmpty.setVisibility(View.VISIBLE);
        imgEmptyIcon.setImageResource(imgRes);
        txtEmptyMsg.setText(msg);
    }

    public void hideEmpty(){
        linEmpty.setVisibility(View.GONE);
    }

}
