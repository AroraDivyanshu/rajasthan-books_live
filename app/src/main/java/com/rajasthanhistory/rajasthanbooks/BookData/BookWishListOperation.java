package com.rajasthanhistory.rajasthanbooks.BookData;

import android.app.Activity;
import android.util.Log;
import android.widget.ImageView;

import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by wscubetech on 14/10/16.
 */
public class BookWishListOperation {

    Activity activity;
    User user;

    public BookWishListOperation(Activity activity){
        this.activity=activity;
        user=new UserDetails(activity).getUserDetails();
    }

    //flag= blank to add   1 to remove
    public void addRemoveFavoriteOkHttp(String flag, final boolean active, final BookModel bookModel, final ImageView imgFavorite) {
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        arrayKeyValueModel.add(new KeyValueModel("product_id", bookModel.getBookId()));
        arrayKeyValueModel.add(new KeyValueModel("flag", flag));

        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_ADD_REMOVE_WISH_LIST, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active) {
                            bookModel.setWished(!bookModel.isWished());
                            imgFavorite.setImageResource(bookModel.isWished() ? R.drawable.ic_favorite_white_24dp : R.drawable.ic_favorite_border_white_24dp);
                        }
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String strResponse = response.body().string();
                    Log.v("ResponseAddRemWish", strResponse);
                } else {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (active) {
                                bookModel.setWished(!bookModel.isWished());
                                imgFavorite.setImageResource(bookModel.isWished() ? R.drawable.ic_favorite_white_24dp : R.drawable.ic_favorite_border_white_24dp);
                            }
                        }
                    });
                }
            }
        });

    }

}
