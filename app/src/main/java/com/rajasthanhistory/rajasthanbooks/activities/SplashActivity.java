package com.rajasthanhistory.rajasthanbooks.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.rajasthanhistory.rajasthanbooks.R;

public class SplashActivity extends AppCompatActivity {

    LinearLayout linMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        linMain=(LinearLayout)findViewById(R.id.linMain);

        Animation animation=AnimationUtils.loadAnimation(this,R.anim.zoom_in_normal);
        linMain.startAnimation(animation);

        Thread thread=new Thread(){
            @Override
            public void run() {
                super.run();
                synchronized (this){
                    try{
                        wait(2500);

                    }catch (Exception e){

                    }

                    Intent intent=new Intent(SplashActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };

        thread.start();

    }
}
