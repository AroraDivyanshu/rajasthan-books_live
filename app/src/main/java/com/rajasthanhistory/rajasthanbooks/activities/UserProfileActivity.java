package com.rajasthanhistory.rajasthanbooks.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.FailureOccurred;
import com.rajasthanhistory.rajasthanbooks.utils.MyValidations;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;
import com.rajasthanhistory.rajasthanbooks.utils.ValidationsListeners;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;

    TextInputLayout inpFirstName, inpPhone, inpEmail;
    EditText etFirstName, etEmail, etPhone;

    DialogMsg dialogMsg, dialogWait;
    MyValidations validations;
    User user;
    FloatingActionButton fab;
    LinearLayout linLoading;
    boolean active;
    public static UserProfileActivity userProfileActivity;

    MenuItem itemUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        init();
        active = true;
        user = new UserDetails(this).getUserDetails();

        toolbarOperation();
        textChangeListeners();
        onClickListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    protected void onStop() {
        active = false;
        super.onStop();
    }

    private void init() {
        linLoading = (LinearLayout) findViewById(R.id.linLoading);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        inpFirstName = (TextInputLayout) findViewById(R.id.inpFirstName);
        inpPhone = (TextInputLayout) findViewById(R.id.inpPhone);
        etPhone = (EditText) findViewById(R.id.etPhone);
        inpEmail = (TextInputLayout) findViewById(R.id.inpEmail);

        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etEmail = (EditText) findViewById(R.id.etEmail);
    }

    private void toolbarOperation() {
        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.title_my_profile));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void textChangeListeners() {
        ValidationsListeners listeners = new ValidationsListeners(this);
        etFirstName.addTextChangedListener(listeners.new MyTextWatcherInputLayout(inpFirstName));
        etPhone.addTextChangedListener(listeners.new MyTextWatcherInputLayout(inpPhone));
        etEmail.addTextChangedListener(listeners.new MyTextWatcherInputLayout(inpEmail));
    }

    private void onClickListeners() {
        fab.setOnClickListener(this);
    }

    public void inputOperationError(TextInputLayout inp, EditText et, String msg) {
        inp.setErrorEnabled(true);
        inp.setError(msg);
        et.requestFocus();
    }

    private void setCurrentUserDetails() {
        setEnable(false);
        linLoading.setVisibility(View.VISIBLE);
        if (new ConnectionDetector(this).isConnectingToInternet()) {
            okHttpViewUserDetails();
        } else {
            linLoading.setVisibility(View.GONE);
        }
    }

    private void setEnable(boolean b) {
        if (active) {
            etFirstName.setText(user.getUserFirstName());
            etEmail.setText(user.getUserEmail());
            etPhone.setText(user.getUserPhone());


            fab.setVisibility(b ? View.GONE : View.VISIBLE);
            itemUpdate.setVisible(b);

            etFirstName.setEnabled(b);
            etEmail.setEnabled(false);
            etPhone.setEnabled(b);

            Log.v("user", user.getUserEmail() + "\n" + user.getUserPhone() + "\n" + user.getUserFirstName());
            if (b) {
                etFirstName.setSelection(user.getUserFirstName().trim().length());
                etPhone.setSelection(user.getUserPhone().trim().length());
            }

        }
    }

    private void okHttpViewUserDetails() {
        fab.setVisibility(View.GONE);
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_USER_DETAILS, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                hideProgress();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                hideProgress();
                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    Log.v("UserDetails", strResponse);
                    try {
                        JSONObject json = new JSONObject(strResponse);
                        if (json.getInt("result") == 1) {
                            JSONObject obj = json.getJSONObject("Message");
                            user.setUserFirstName(obj.getString("user_fname"));
                            user.setUserEmail(obj.getString("user_email"));
                            user.setUserPhone(obj.getString("user_phone"));
                            UserDetails userDetails = new UserDetails(UserProfileActivity.this);
                            userDetails.setUserDetails(user);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    user = new UserDetails(UserProfileActivity.this).getUserDetails();
                                    setEnable(false);
                                }
                            });
                        }
                    } catch (Exception e) {

                    }
                }
            }
        });
    }

    private void hideProgress() {
        if (active) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    linLoading.setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_user_profile, menu);
        itemUpdate = menu.findItem(R.id.itemUpdate);
        itemUpdate.setVisible(false);
        setCurrentUserDetails();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.itemUpdate:
                if (isValidated()) {
                    if (new ConnectionDetector(this).isConnectingToInternet()) {
                        okHttpEditUser();
                    } else {
                        dialogMsg = new DialogMsg(this);
                        dialogMsg.showDialog(getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogMsg.getDialog().dismiss();
                            }
                        });
                    }
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isValidated() {
        validations = new MyValidations(this);
        String strFirstName, strPhone;
        strFirstName = etFirstName.getText().toString().trim();
        strPhone = etPhone.getText().toString().trim();

        if (strFirstName.isEmpty() || !validations.checkName(strFirstName)) {
            inputOperationError(inpFirstName, etFirstName, "Please enter a valid name");
            return false;
        }


        if (strPhone.isEmpty() || !validations.checkMobile(strPhone)) {
            inputOperationError(inpPhone, etPhone, "Please enter a valid phone number");
            return false;
        }

        user.setUserFirstName(strFirstName);
        user.setUserPhone(strPhone);

        return true;
    }

    private void okHttpEditUser() {
        dialogWait=new DialogMsg(this);
        dialogWait.showPleaseWaitDialog();

        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        arrayKeyValueModel.add(new KeyValueModel("user_fname", user.getUserFirstName()));
        arrayKeyValueModel.add(new KeyValueModel("user_phone", user.getUserPhone()));

        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_EDIT_USER, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogWait.getDialog().dismiss();
                    }
                });
                FailureOccurred failureOccurred = new FailureOccurred(UserProfileActivity.this);
                failureOccurred.showFailureDialog(false);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogWait.getDialog().dismiss();
                    }
                });

                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    Log.v("UserEditResponse", strResponse);
                    UserDetails userDetails=new UserDetails(UserProfileActivity.this);
                    userDetails.setUserDetails(user);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setEnable(false);
                            dialogMsg=new DialogMsg(UserProfileActivity.this);
                            dialogMsg.showDialog("Profile successfully updated", R.drawable.ic_success, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogMsg.getDialog().dismiss();
                                }
                            });
                        }
                    });


                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtRegister:

                break;

            case R.id.fab:
                setEnable(true);
                break;
        }
    }
}
