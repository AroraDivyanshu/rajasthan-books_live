package com.rajasthanhistory.rajasthanbooks.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.rajasthanhistory.rajasthanbooks.BookData.EmptyScreen;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.adapter.OrderAdapter;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.model.OrderModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.FailureOccurred;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MyOrdersActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressWheel progressWheel;

    ArrayList<OrderModel> arrayOrderModel = new ArrayList<>();
    OrderAdapter adapter;

    User user;
    DialogMsg dialogMsg;
    boolean active;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        init();
        toolbarOperation();

        active = true;
        user = new UserDetails(this).getUserDetails();

        if (new ConnectionDetector(this).isConnectingToInternet()) {
            okHttpMyOrders();
        } else {
            progressWheel.setVisibility(View.GONE);
            dialogMsg = new DialogMsg(this);
            dialogMsg.showDialog(getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogMsg.getDialog().dismiss();
                    finish();
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    protected void onStop() {
        active = false;
        super.onStop();
    }

    private void init() {
        progressWheel = (ProgressWheel) findViewById(R.id.progressBar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void toolbarOperation() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.title_my_orders));
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void okHttpMyOrders() {
        progressWheel.setVisibility(View.VISIBLE);
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_VIEW_ORDER, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active) {
                            progressWheel.setVisibility(View.GONE);
                            FailureOccurred occurred = new FailureOccurred(MyOrdersActivity.this);
                            occurred.showFailureDialog(true);
                        }
                    }
                });

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    try {
                        Log.v("Response", strResponse);
                        JSONObject json = new JSONObject(strResponse);
                        if (json.getInt("result") == 1) {
                            JSONArray array = json.getJSONArray("Message");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject obj = array.getJSONObject(i);
                                OrderModel orderModel = new OrderModel();
                                orderModel.setOrderId(obj.getString("checkout_id"));
                                orderModel.setOrderCodeName(obj.getString("order_name"));
                                orderModel.setOrderTotalAmount(obj.getString("total_amount"));
                                orderModel.setOrderPaymentType(obj.getString("payment_type"));
                                orderModel.setOrderPaymentStatus(obj.getString("payment_status"));
                                orderModel.setOrderDate(obj.getString("date"));

                                ArrayList<BookModel> arrayBookModel = new ArrayList<>();
                                ;

                                try {
                                    JSONArray jsonArray = obj.getJSONArray("details");

                                    for (int j = 0; j < jsonArray.length(); j++) {
                                        JSONObject object = jsonArray.getJSONObject(j);
                                        BookModel bookModel = new BookModel();
                                        bookModel.setBookId(object.getString("cart_product_id"));
                                        bookModel.setBookTitle(object.getString("product_name"));
                                        bookModel.setBookImage(object.getString("product_popimage"));
                                        bookModel.setBookAuthor(object.getString("author"));
                                        bookModel.setBookDiscountedPrice(object.getString("cart_price"));
                                        bookModel.setBookType(object.getString("book_type"));
                                        bookModel.setBookUrlFileName(object.getString("upload_ebook"));
                                        bookModel.setBookSize(object.getString("product_length"));
                                        arrayBookModel.add(bookModel);
                                    }
                                } catch (Exception e) {
                                    Log.v("JsonProblem", "" + e);
                                }

                                orderModel.setArrayBookModel(arrayBookModel);
                                arrayOrderModel.add(orderModel);
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (active) {
                                        adapter = new OrderAdapter(MyOrdersActivity.this, arrayOrderModel);
                                        recyclerView.setAdapter(adapter);
                                    }
                                }
                            });


                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (active) {
                                        EmptyScreen emptyScreen = new EmptyScreen(MyOrdersActivity.this);
                                        emptyScreen.setIconAndMsg("No orders placed yet", R.drawable.ic_assignment_turned_in_black_24dp);
                                    }
                                }
                            });

                        }
                    } catch (Exception e) {
                        Log.v("Exception", "" + e);
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active)
                            progressWheel.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
