package com.rajasthanhistory.rajasthanbooks.activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.rajasthanhistory.rajasthanbooks.BookData.EmptyScreen;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.adapter.OrderDetailBookAdapter;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.GetSetSharedPrefs;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;
import com.rajasthanhistory.rajasthanbooks.utils.PreferencesKeys;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MyBooksActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;

    ProgressWheel progressBar;

    GetSetSharedPrefs prefs;

    ArrayList<BookModel> arrayBookModel = new ArrayList<>();
    OrderDetailBookAdapter adapter;

    User user;
    boolean active;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_books);
        init();
        active = true;
        toolbarOperation();

        user = new UserDetails(this).getUserDetails();

        getUserBooks();
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    protected void onStop() {
        active = false;
        super.onStop();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar = (ProgressWheel) findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void toolbarOperation() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("My Books");
    }

    private void okHttpViewUserBooks() {
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_VIEW_MY_BOOKS, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                hideProgressBar();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    prefs = new GetSetSharedPrefs(MyBooksActivity.this, PreferencesKeys.PREFERENCE_SETTINGS);
                    prefs.putData(PreferencesKeys.PREF_MY_BOOKS, strResponse);
                    parseResponseBooks(strResponse);
                } else {
                    noBookFound(R.drawable.ic_error_internet, getString(R.string.network_error));
                }

                hideProgressBar();


            }
        });
    }

    private void hideProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (active)
                    progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void noBookFound(final int resource, final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (active && arrayBookModel.size() == 0) {
                    EmptyScreen emptyScreen = new EmptyScreen(MyBooksActivity.this);
                    emptyScreen.setIconAndMsg(msg, resource);
                }
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    private void getUserBooks() {
        prefs = new GetSetSharedPrefs(this, PreferencesKeys.PREFERENCE_SETTINGS);
        String response = prefs.getData(PreferencesKeys.PREF_MY_BOOKS);
        if (new ConnectionDetector(this).isConnectingToInternet()) {
            okHttpViewUserBooks();
        } else {
            if (response.trim().length() < 2)
                noBookFound(R.drawable.ic_error_internet, getString(R.string.connection_error));
            else
                parseResponseBooks(response);
        }

    }

    private void parseResponseBooks(String strResponse) {
        try {
            JSONObject json = new JSONObject(strResponse);
            Log.v("ResponseBooks", strResponse);
            if (json.getInt("result") == 1) {
                JSONArray array = json.getJSONArray("Message");
                if (array.length() > 0)
                    arrayBookModel.clear();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    BookModel model = new BookModel();
                    model.setBookId(obj.getString("product_id"));
                    model.setBookImage(obj.getString("product_popimage"));
                    model.setBookTitle(obj.getString("product_name"));
                    model.setBookAuthor(obj.getString("author"));
                    model.setBookUrlFileName(obj.getString("ebook_url"));
                    model.setBookSize(obj.getString("product_length"));
                    arrayBookModel.add(model);
                }

                setResponseInRecyclerView();
            } else {

                if (new ConnectionDetector(MyBooksActivity.this).isConnectingToInternet())
                    noBookFound(R.drawable.ic_book_placeholder, "No books purchased yet");
                else
                    noBookFound(R.drawable.ic_error_internet, getString(R.string.connection_error));
            }
        } catch (Exception e) {

        }
    }

    private void setResponseInRecyclerView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (active) {
                    progressBar.setVisibility(View.GONE);

                    if (adapter == null) {
                        adapter = new OrderDetailBookAdapter(MyBooksActivity.this, arrayBookModel, "MyBooks");
                        ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(adapter);
                        recyclerView.setAdapter(animationAdapter);
                    } else {
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
