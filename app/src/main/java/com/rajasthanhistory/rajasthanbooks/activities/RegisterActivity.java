package com.rajasthanhistory.rajasthanbooks.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.FailureOccurred;
import com.rajasthanhistory.rajasthanbooks.utils.MyValidations;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;
import com.rajasthanhistory.rajasthanbooks.utils.ValidationsListeners;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    TextView txtRegister;
    LinearLayout linParent;

    CardView cardRegister;
    TextInputLayout inpFirstName, inpPhone, inpEmail, inpPassword, inpConfirmPassword;
    EditText etFirstName, etEmail, etPhone, etPassword, etConfirmPassword;

    DialogMsg dialogMsg;
    MyValidations validations;
    public static RegisterActivity registerActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
        registerActivity = this;

        toolbarOperation();

        textChangeListeners();
        onClickListeners();


    }

    private void init() {
        linParent = (LinearLayout) findViewById(R.id.linParent);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtRegister = (TextView) findViewById(R.id.txtRegister);
        cardRegister = (CardView) findViewById(R.id.cardRegister);

        inpFirstName = (TextInputLayout) findViewById(R.id.inpFirstName);
        inpPhone = (TextInputLayout) findViewById(R.id.inpPhone);
        etPhone = (EditText) findViewById(R.id.etPhone);
        inpEmail = (TextInputLayout) findViewById(R.id.inpEmail);
        inpPassword = (TextInputLayout) findViewById(R.id.inpPassword);
        inpConfirmPassword = (TextInputLayout) findViewById(R.id.inpConfirmPassword);

        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
    }

    private void toolbarOperation() {
        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.title_registration));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void textChangeListeners() {
        ValidationsListeners listeners = new ValidationsListeners(this);
        etFirstName.addTextChangedListener(listeners.new MyTextWatcherInputLayout(inpFirstName));
        etPhone.addTextChangedListener(listeners.new MyTextWatcherInputLayout(inpPhone));
        etEmail.addTextChangedListener(listeners.new MyTextWatcherInputLayout(inpEmail));
        etPassword.addTextChangedListener(listeners.new MyTextWatcherInputLayout(inpPassword));
        etConfirmPassword.addTextChangedListener(listeners.new MyTextWatcherInputLayout(inpConfirmPassword, etPassword));
    }

    private void onClickListeners() {
        txtRegister.setOnClickListener(this);
    }

    public void inputOperationError(TextInputLayout inp, EditText et, String msg) {
        inp.setErrorEnabled(true);
        inp.setError(msg);
        et.requestFocus();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtRegister:
                validations = new MyValidations(this);
                String strFirstName, strLastName, strPhone, strEmail, strPassword, strConfirmPassword;
                strFirstName = etFirstName.getText().toString().trim();
                strPhone = etPhone.getText().toString().trim();
                strEmail = etEmail.getText().toString().trim();
                strPassword = etPassword.getText().toString();
                strConfirmPassword = etConfirmPassword.getText().toString();

                if (strFirstName.isEmpty() || !validations.checkName(strFirstName)) {
                    inputOperationError(inpFirstName, etFirstName, "Please enter a valid name");
                    return;
                }


                if (strPhone.isEmpty() || !validations.checkMobile(strPhone)) {
                    inputOperationError(inpPhone, etPhone, "Please enter a valid phone number");
                    return;
                }

                if (!validations.checkEmail(strEmail)) {
                    inputOperationError(inpEmail, etEmail, "Please enter a valid e-mail");
                    return;
                }

                if (strPassword.length() < 3) {
                    inputOperationError(inpPassword, etPassword, "Please enter a valid password at least 3 character long");
                    return;
                }

                if (!strPassword.equals(strConfirmPassword)) {
                    inputOperationError(inpConfirmPassword, etConfirmPassword, "Passwords do not match");
                    return;
                }

                User user = new User();
                user.setUserFirstName(strFirstName);
                user.setUserPhone(strPhone);
                user.setUserEmail(strEmail);
                user.setUserPassword(strPassword);

                registerOkHttp(user);

                break;
        }
    }

    private void registerOkHttp(final User user) {
        dialogMsg = new DialogMsg(this);
        dialogMsg.showPleaseWaitDialog();

        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("first_name", user.getUserFirstName()));
        arrayKeyValueModel.add(new KeyValueModel("email_id", user.getUserEmail()));
        arrayKeyValueModel.add(new KeyValueModel("number", user.getUserPhone()));
        arrayKeyValueModel.add(new KeyValueModel("password", user.getUserPassword()));

        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_REGISTRATION, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogMsg.getDialog().dismiss();
                    }
                });
                FailureOccurred failureOccurred = new FailureOccurred(RegisterActivity.this);
                failureOccurred.showFailureDialog(false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogMsg.getDialog().dismiss();
                    }
                });
                if (response.isSuccessful()) {
                    final String strResponse = Html.fromHtml(response.body().string()).toString();
                    Log.v("ResponseRegister", strResponse);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject json = new JSONObject(strResponse);
                                if (json.getInt("result") == 1) {
                                    String strUserId = json.getString("user_id");
                                    user.setUserId(strUserId);
                                    UserDetails userDetails = new UserDetails(RegisterActivity.this);
                                    userDetails.setUserDetails(user);

                                    dialogMsg = new DialogMsg(RegisterActivity.this);
                                    dialogMsg.showDialog("Registration successful\nTap on 'OK' to continue", R.drawable.ic_success, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialogMsg.getDialog().dismiss();
                                            if (LoginActivity.loginActivity != null)
                                                LoginActivity.loginActivity.finish();
                                            if (MainActivity.mainActivity != null)
                                                MainActivity.mainActivity.finish();
                                            if(BookDetailActivity.bookDetailActivity!=null)
                                                BookDetailActivity.bookDetailActivity.finish();

                                            Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    });
                                }else{
                                    dialogMsg = new DialogMsg(RegisterActivity.this);
                                    dialogMsg.showDialog("User with this email or phone number already exists\nPlease choose a different email or phone number", R.drawable.ic_error_general, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialogMsg.getDialog().dismiss();
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                Log.v("ExceptionRegister", "" + e);
                            }
                        }
                    });
                }
            }
        });

    }
}
