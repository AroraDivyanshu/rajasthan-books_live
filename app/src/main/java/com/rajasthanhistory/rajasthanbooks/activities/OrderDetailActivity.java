package com.rajasthanhistory.rajasthanbooks.activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.adapter.OrderDetailBookAdapter;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.OrderModel;

import java.util.ArrayList;

public class OrderDetailActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtOrderNameCode, txtOrderTotal, txtDate;
    RecyclerView recyclerView;

    OrderModel orderModel;
    ArrayList<BookModel> arrayBookModel = new ArrayList<>();
    OrderDetailBookAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        init();
        toolbarOperation();
        getOrderDetail();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtDate = (TextView) findViewById(R.id.txtDate);
        txtOrderNameCode = (TextView) findViewById(R.id.txtOrderNameCode);
        txtOrderTotal = (TextView) findViewById(R.id.txtOrderTotal);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
    }

    private void toolbarOperation() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Order Detail");
    }

    private void getOrderDetail() {
        orderModel = (OrderModel) getIntent().getExtras().getSerializable("OrderModel");
        txtOrderNameCode.setText("Order id: " + orderModel.getOrderCodeName());
        txtOrderTotal.setText(getString(R.string.rs) + orderModel.getOrderTotalAmount());
        txtDate.setText(orderModel.getOrderDate());

        arrayBookModel.clear();
        arrayBookModel = orderModel.getArrayBookModel();
        adapter = new OrderDetailBookAdapter(this, arrayBookModel,"OrderDetail");
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
