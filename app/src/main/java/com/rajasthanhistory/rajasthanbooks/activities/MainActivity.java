package com.rajasthanhistory.rajasthanbooks.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.rajasthanhistory.rajasthanbooks.BookData.BookJsonParse;
import com.rajasthanhistory.rajasthanbooks.BookData.CartBadgeCounter;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.custom.CustomFont;
import com.rajasthanhistory.rajasthanbooks.custom.FontsOverride;
import com.rajasthanhistory.rajasthanbooks.fragments.NavDrawerFragment;
import com.rajasthanhistory.rajasthanbooks.fragments.TabBarHomeFragment;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.GetSetSharedPrefs;
import com.rajasthanhistory.rajasthanbooks.utils.InitializeFragment;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;
import com.rajasthanhistory.rajasthanbooks.utils.PreferencesKeys;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements NavDrawerFragment.FragmentDrawerListener, View.OnClickListener {

    private NavDrawerFragment drawerFragment;
    private DrawerLayout drawerLayout;
    public static Toolbar toolbar;

    public static MainActivity mainActivity;
    //for badge counter viewing
    ArrayList<BookModel> arrayBookModel = new ArrayList<>();
    CartBadgeCounter badgeCounter;


    GetSetSharedPrefs prefs;

    User user;

    OkHttpCalls okHttpCalls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_main);
        user = new UserDetails(this).getUserDetails();
        init();

        mainActivity = this;

        toolbarOperation();
        setUpNavDrawer();

        if (new ConnectionDetector(this).isConnectingToInternet())
            resetOfflineStringResponse();

        MainActivity.toolbar.setTitle(getString(R.string.app_name));
        new InitializeFragment(MainActivity.this, "add", "yes", "TabBarHomeFragment").initFragment(new TabBarHomeFragment(), getSupportFragmentManager());

        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/montserrat_light.otf");


    }

    public void resetOfflineStringResponse() {
        prefs = new GetSetSharedPrefs(this, PreferencesKeys.PREFERENCE_SETTINGS);
        prefs.putData(PreferencesKeys.PREF_CATEGORY_HOME, "");
        prefs.putData(PreferencesKeys.PREF_FEATURED_HOME, "");
        prefs.putData(PreferencesKeys.PREF_NEW_HOME, "");
        prefs.putData(PreferencesKeys.PREF_FEATURED_ALL, "");
        prefs.putData(PreferencesKeys.PREF_NEW_ALL, "");
        prefs.putData(PreferencesKeys.PREF_SLIDER_HOME, "");
        prefs.putData(PreferencesKeys.PREF_SALE_ALL, "");
        prefs.putData(PreferencesKeys.PREF_FREE_ALL, "");
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (new ConnectionDetector(this).isConnectingToInternet())
            okHttpViewCartForCounter();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

    }

    private void toolbarOperation() {
        setSupportActionBar(toolbar);
        applyFontForToolbarTitle(this);
    }

    private void applyFontForToolbarTitle(Activity context) {
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                Typeface titleFont = CustomFont.setFontBold(getAssets());
                tv.setTypeface(titleFont);
            }
        }
    }

    private void setUpNavDrawer() {
        drawerFragment = (NavDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.NavDrawerFragment);
        drawerFragment.setDrawerListener(this);
        drawerFragment.setUp(drawerLayout, toolbar);
    }

    @Override
    public void onDrawerItemSelected(int position) {
        Log.v("Position: ", "" + position);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            return;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() == 2) {
            fragmentManager.popBackStack();
            fragmentManager.popBackStack();
            MainActivity.toolbar.setTitle(getString(R.string.app_name));
            new InitializeFragment(MainActivity.this, "add", "yes", "TabBarHomeFragment").initFragment(new TabBarHomeFragment(), getSupportFragmentManager());
        } else if (fragmentManager.getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (user.getUserId().trim().equalsIgnoreCase("0"))
            inflater.inflate(R.menu.menu_before_login, menu);
        else {
            saveUserDetailsAfterLogin();
            inflater.inflate(R.menu.menu_main_after_login, menu);
            MenuItem menuItem = menu.findItem(R.id.itemCart);
            badgeCounter = new CartBadgeCounter(this, menuItem);
            badgeCounter.initiateView();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final Intent intent;
        Intent intentCartWishList = new Intent(MainActivity.this, CartWishListActivity.class);
        switch (item.getItemId()) {
            case R.id.login:
                intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.register:
                intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
                break;
            case R.id.itemMyProfile:
                intent = new Intent(MainActivity.this, UserProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.itemLogOut:
                UserDetails userDetails = new UserDetails(this);
                userDetails.setUserDetails(new User());
                Toast.makeText(this, "Logged out successfully", Toast.LENGTH_LONG).show();
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.itemOrders:
                intent = new Intent(this, MyOrdersActivity.class);
                startActivity(intent);
                break;
            case R.id.itemMyBooks:
                intent = new Intent(this, MyBooksActivity.class);
                startActivity(intent);
                break;
            case R.id.itemWishList:
                intentCartWishList.putExtra("ComingFrom", getString(R.string.title_wish_list));
                startActivity(intentCartWishList);
                break;
            case R.id.itemSearch:
                intent = new Intent(this, SearchActivity.class);
                startActivity(intent);
                break;

            case R.id.privacyPolicy:
                intent = new Intent(this, PrivacyPolicy.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void okHttpViewCartForCounter() {
        if (!user.getUserId().trim().equals("0")) {
            String url = Urls.API_VIEW_CART;
            final ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
            arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
            OkHttpCalls calls = new OkHttpCalls(url, arrayKeyValueModel);
            calls.initiateCall(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            String res = Html.fromHtml(response.body().string()).toString();
                            BookJsonParse jsonParse = new BookJsonParse(MainActivity.this, getString(R.string.title_cart));
                            arrayBookModel = jsonParse.parseBookResponseGetArray(res);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    badgeCounter.setBadgeCount(arrayBookModel.size());
                                }
                            });
                        } catch (Exception e) {
                            Log.v("Exception", "" + e);
                        }
                    }
                }
            });
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

    private void saveUserDetailsAfterLogin() {
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_USER_DETAILS, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    Log.v("UserDetails", strResponse);
                    try {
                        JSONObject json = new JSONObject(strResponse);
                        if (json.getInt("result") == 1) {
                            JSONObject obj = json.getJSONObject("Message");
                            user.setUserFirstName(obj.getString("user_fname"));
                            user.setUserEmail(obj.getString("user_email"));
                            user.setUserPhone(obj.getString("user_phone"));
                            UserDetails userDetails = new UserDetails(MainActivity.this);
                            userDetails.setUserDetails(user);
                        }
                    } catch (Exception e) {

                    }
                }
            }
        });
    }

}
