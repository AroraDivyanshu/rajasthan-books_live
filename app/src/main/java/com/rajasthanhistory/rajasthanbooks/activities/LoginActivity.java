package com.rajasthanhistory.rajasthanbooks.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.FailureOccurred;
import com.rajasthanhistory.rajasthanbooks.utils.MyValidations;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;
import com.rajasthanhistory.rajasthanbooks.utils.ValidationsListeners;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    TextView txtLogin, txtRegister;

    LinearLayout linParent;

    CardView cardLogin;
    TextInputLayout inpEmail, inpPassword;
    EditText etEmail, etPassword;

    MyValidations validations;
    DialogMsg dialogMsg;
    public static LoginActivity loginActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        loginActivity = this;
        toolbarOperation();
        textChangeListeners();
        onClickListeners();


    }

    private void init() {
        linParent=(LinearLayout)findViewById(R.id.linParent);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtLogin = (TextView) findViewById(R.id.txtLogin);
        txtRegister = (TextView) findViewById(R.id.txtRegister);

        cardLogin = (CardView) findViewById(R.id.cardLogin);

        inpEmail = (TextInputLayout) findViewById(R.id.inpEmail);
        inpPassword = (TextInputLayout) findViewById(R.id.inpPassword);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
    }

    private void toolbarOperation() {
        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.title_login));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void textChangeListeners() {
        ValidationsListeners listeners = new ValidationsListeners(this);
        etEmail.addTextChangedListener(listeners.new MyTextWatcherInputLayout(inpEmail));
        etPassword.addTextChangedListener(listeners.new MyTextWatcherInputLayout(inpPassword));
    }

    private void onClickListeners() {
        txtLogin.setOnClickListener(this);
        txtRegister.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtLogin:
                validations = new MyValidations(this);
                String strEmail,strPassword;
                strEmail=etEmail.getText().toString().trim();
                strPassword=etPassword.getText().toString();


                if(!validations.checkEmail(strEmail)){
                    inputOperationError(inpEmail,etEmail,"Please enter a valid e-mail");
                    return;
                }

                if(strPassword.length()<3){
                    inputOperationError(inpPassword,etPassword,"Please enter a valid password at least 3 character long");
                    return;
                }

                if(!new ConnectionDetector(this).isConnectingToInternet()){
                    dialogMsg=new DialogMsg(this);
                    dialogMsg.showDialog(getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogMsg.getDialog().dismiss();
                        }
                    });
                    return;
                }

                User user=new User();
                user.setUserEmail(strEmail);
                user.setUserPassword(strPassword);
                loginOkHttp(user);

                break;
            case R.id.txtRegister:
                Intent intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                break;
        }
    }

    public void inputOperationError(TextInputLayout inp, EditText et, String msg) {
        inp.setErrorEnabled(true);
        inp.setError(msg);
        et.requestFocus();
    }

    private void loginOkHttp(final User user) {
        dialogMsg = new DialogMsg(this);
        dialogMsg.showPleaseWaitDialog();

        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("email_id", user.getUserEmail()));
        arrayKeyValueModel.add(new KeyValueModel("password", user.getUserPassword()));

        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_LOGIN, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogMsg.getDialog().dismiss();
                    }
                });
                FailureOccurred failureOccurred = new FailureOccurred(LoginActivity.this);
                failureOccurred.showFailureDialog(false);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogMsg.getDialog().dismiss();
                    }
                });
                if (response.isSuccessful()) {
                    final String strResponse = Html.fromHtml(response.body().string()).toString();
                    Log.v("ResponseLogin", strResponse);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject json = new JSONObject(strResponse);
                                if (json.getInt("result") == 1) {
                                    String strUserId = json.getString("user_id");
                                    user.setUserId(strUserId);
                                    UserDetails userDetails = new UserDetails(LoginActivity.this);
                                    userDetails.setUserDetails(user);
                                    if (RegisterActivity.registerActivity != null)
                                        RegisterActivity.registerActivity.finish();
                                    if (MainActivity.mainActivity != null)
                                        MainActivity.mainActivity.finish();
                                    if(BookDetailActivity.bookDetailActivity!=null)
                                        BookDetailActivity.bookDetailActivity.finish();

                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }else{
                                    dialogMsg = new DialogMsg(LoginActivity.this);
                                    dialogMsg.showDialog("Invalid e-mail or password", R.drawable.ic_error_general, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialogMsg.getDialog().dismiss();
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                Log.v("ExceptionLogin", "" + e);
                            }
                        }
                    });
                }
            }
        });

    }
}
