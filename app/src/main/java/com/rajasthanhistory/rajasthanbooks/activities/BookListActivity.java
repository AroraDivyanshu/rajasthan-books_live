package com.rajasthanhistory.rajasthanbooks.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.rajasthanhistory.rajasthanbooks.BookData.BookJsonParse;
import com.rajasthanhistory.rajasthanbooks.BookData.EmptyScreen;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.adapter.BookCartWishListAdapter;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.dialogs.MyDialog;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.CategoryModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.model.SubCategoryModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.FailureOccurred;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;

import java.io.IOException;
import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class BookListActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    RecyclerView recyclerView;

    RelativeLayout relSort, relFilter;
    TextView txtSortValue;
    String radioSelected = "Book Title";

    ProgressWheel progressWheel;
    CategoryModel categoryModel;
    SubCategoryModel subCategoryModel;

    ArrayList<BookModel> arrayBookModel = new ArrayList<>();
    BookCartWishListAdapter bookAdapter;

    boolean active;
    String categoryOrSubCategory = "";
    Dialog dialogSorting;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);
        init();
        active = true;
        toolbarOperation();
        onClickListeners();
        getCategoryDetail();
    }

    private void init() {
        relSort = (RelativeLayout) findViewById(R.id.relSort);
        relFilter = (RelativeLayout) findViewById(R.id.relFilter);
        txtSortValue = (TextView) findViewById(R.id.txtSortValue);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressWheel = (ProgressWheel) findViewById(R.id.progressBar);
        progressWheel.setVisibility(View.GONE);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void toolbarOperation() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void onClickListeners() {
        relSort.setOnClickListener(this);
        relFilter.setOnClickListener(this);
    }

    private void getCategoryDetail() {
        categoryOrSubCategory = getIntent().getExtras().getString("CategorySubCategory");
        String title = "", id = "";
        if (categoryOrSubCategory.equalsIgnoreCase("Category")) {
            categoryModel = (CategoryModel) getIntent().getExtras().getSerializable("CategoryModel");
            title = categoryModel.getCategoryName();
            id = categoryModel.getCategoryId();
        } else {
            subCategoryModel = (SubCategoryModel) getIntent().getExtras().getSerializable("SubCategoryModel");
            title = subCategoryModel.getSubCategoryName();
            id = subCategoryModel.getSubcategoryId();
        }
        toolbar.setTitle(title);
        if (new ConnectionDetector(this).isConnectingToInternet()) {
            okHttpViewCategorizedBooks(id);
        } else {
            internetErrorDialog();
        }

        txtSortValue.setText(radioSelected);
    }

    private void internetErrorDialog() {
        final DialogMsg dialogMsg = new DialogMsg(this);
        dialogMsg.showDialog(getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.getDialog().dismiss();
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    protected void onStop() {
        active = false;
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void okHttpViewCategorizedBooks(String id) {
        progressWheel.setVisibility(View.VISIBLE);
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("product_category", id));
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_VIEW_BOOKS_CATEGORIZED, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressWheel.setVisibility(View.GONE);
                    }
                });
                FailureOccurred failureOccurred = new FailureOccurred(BookListActivity.this);
                failureOccurred.showFailureDialog(true);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    BookJsonParse jsonParse = new BookJsonParse(BookListActivity.this, "Categorized");
                    arrayBookModel = jsonParse.parseBookResponseGetArray(strResponse);
                }
                setBooksInRecyclerView();
            }
        });
    }

    private void setBooksInRecyclerView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (active) {
                    if (arrayBookModel.size() > 0) {
                        bookAdapter = new BookCartWishListAdapter(BookListActivity.this, arrayBookModel, "BookList", active);
                        recyclerView.setAdapter(new ScaleInAnimationAdapter(bookAdapter));
                    } else {
                        EmptyScreen emptyScreen = new EmptyScreen(BookListActivity.this);
                        emptyScreen.setIconAndMsg("No books found on server of this particular category", R.drawable.ic_book_placeholder);
                    }

                    progressWheel.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relSort:
                if (arrayBookModel.size() > 1) {
                    dialogSorting = new MyDialog(this).getMyDialog(R.layout.dialog_sorting_filter);
                    RadioButton radioBookTitle, radioAuthorName, radioPopularity, radioPriceLowToHigh, radioPriceHighToLow;
                    radioBookTitle = (RadioButton) dialogSorting.findViewById(R.id.radioBookTitle);
                    radioAuthorName = (RadioButton) dialogSorting.findViewById(R.id.radioAuthorName);
                    radioPopularity = (RadioButton) dialogSorting.findViewById(R.id.radioPopularity);
                    radioPriceLowToHigh = (RadioButton) dialogSorting.findViewById(R.id.radioPriceLowToHigh);
                    radioPriceHighToLow = (RadioButton) dialogSorting.findViewById(R.id.radioPriceHighToLow);

                    radioBookTitle.setOnClickListener(this);
                    radioAuthorName.setOnClickListener(this);
                    radioPopularity.setOnClickListener(this);
                    radioPriceLowToHigh.setOnClickListener(this);
                    radioPriceHighToLow.setOnClickListener(this);

                    radioBookTitle.setChecked(radioSelected.equalsIgnoreCase(radioBookTitle.getText().toString().trim()));
                    radioAuthorName.setChecked(radioSelected.equalsIgnoreCase(radioAuthorName.getText().toString().trim()));
                    radioPopularity.setChecked(radioSelected.equalsIgnoreCase(radioPopularity.getText().toString().trim()));
                    radioPriceLowToHigh.setChecked(radioSelected.equalsIgnoreCase(radioPriceLowToHigh.getText().toString().trim()));
                    radioPriceHighToLow.setChecked(radioSelected.equalsIgnoreCase(radioPriceHighToLow.getText().toString().trim()));

                    dialogSorting.show();
                }
                break;
            case R.id.radioAuthorName:
            case R.id.radioBookTitle:
            case R.id.radioPopularity:
            case R.id.radioPriceLowToHigh:
            case R.id.radioPriceHighToLow:
                radioSelected = ((RadioButton) view).getText().toString().trim();
                txtSortValue.setText(radioSelected);
                if (dialogSorting != null && dialogSorting.isShowing())
                    dialogSorting.dismiss();

                if (!new ConnectionDetector(this).isConnectingToInternet()) {
                    internetErrorDialog();
                    return;
                }

                String flag = "";
                switch (radioSelected) {
                    case "Book Title":
                        flag = "4";
                        break;
                    case "Author Name":
                        flag = "3";
                        break;
                    case "Popularity":
                        flag = "5";
                        break;
                    case "Price: Low to High":
                        flag = "1";
                        break;
                    case "Price: High to Low":
                        flag = "2";
                        break;
                }

                okHttpSorting(flag);

                break;
        }
    }

    /*flag =1 for price Asc
    flag =2 for price DESC
    flag =3 for authorname Asc
    flag =4 for book type Asc
    flag =5 for popularity Asc*/
    private void okHttpSorting(String flag) {
        recyclerView.setVisibility(View.GONE);
        progressWheel.setVisibility(View.VISIBLE);
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("flag", flag));
        String id = categoryOrSubCategory.equalsIgnoreCase("Category") ? categoryModel.getCategoryId() : subCategoryModel.getSubcategoryId();
        arrayKeyValueModel.add(new KeyValueModel("category_id", id));
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_SORTING, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressWheel.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        if (response.isSuccessful()) {
                            String strResponse = Html.fromHtml(response.body().string()).toString();
                            BookJsonParse jsonParse = new BookJsonParse(BookListActivity.this, "Sorting");
                            arrayBookModel = jsonParse.parseBookResponseGetArray(strResponse);
                        }
                        setBooksInRecyclerView();
                    } catch (Exception e) {
                        Log.v("ExceptionSort", "" + e);
                    }
                }
            }
        });
    }
}
