package com.rajasthanhistory.rajasthanbooks.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.rajasthanhistory.rajasthanbooks.BookData.BookJsonParse;
import com.rajasthanhistory.rajasthanbooks.BookData.EmptyScreen;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.adapter.BookCartWishListAdapter;
import com.rajasthanhistory.rajasthanbooks.adapter.BooksHomePageAdapter;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.FailureOccurred;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class CartWishListActivity extends AppCompatActivity implements View.OnClickListener {

    String comingFrom = ""; //string file titles  Cart   Wish List

    Toolbar toolbar;
    RecyclerView recyclerView;
    ArrayList<BookModel> arrayBookModel = new ArrayList<>();
    BookCartWishListAdapter adapter;

    CardView cardView;
    public static TextView txtTotalAmount;
    RelativeLayout relPlaceOrder;
    ProgressWheel progressBar;

    public static float total = 0;
    boolean active;

    User user;
    DialogMsg dialogMsg;

    public static CartWishListActivity cartWishListActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_wish_list);
        init();

        cartWishListActivity=this;

        active = true;
        user = new UserDetails(this).getUserDetails();

        comingFrom = getIntent().getExtras().getString("ComingFrom");
        toolbarOperation();
        onClickListeners();

        if (new ConnectionDetector(this).isConnectingToInternet()) {
            okHttpViewCartWishList();
        } else {
            dialogMsg = new DialogMsg(this);
            dialogMsg.showDialog(getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogMsg.getDialog().dismiss();
                    finish();
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    protected void onStop() {
        active = false;
        super.onStop();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        cardView = (CardView) findViewById(R.id.card);
        cardView.setVisibility(View.GONE);

        txtTotalAmount = (TextView) findViewById(R.id.txtTotalAmount);
        progressBar = (ProgressWheel) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        relPlaceOrder = (RelativeLayout) findViewById(R.id.relPlaceOrder);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void toolbarOperation() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(comingFrom);
    }

    public void onClickListeners() {
        relPlaceOrder.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relPlaceOrder:
                if (new ConnectionDetector(this).isConnectingToInternet()) {
                    Intent intent=new Intent(this,MakePaymentActivity.class);
                    intent.putExtra("ArrayBookModel",arrayBookModel);
                    startActivity(intent);
                } else {
                    dialogMsg = new DialogMsg(this);
                    dialogMsg.showDialog(getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogMsg.getDialog().dismiss();
                        }
                    });
                }
                break;
        }
    }

    private void okHttpViewCartWishList() {
        progressBar.setVisibility(View.VISIBLE);
        String url = comingFrom.equals(getString(R.string.title_cart)) ? Urls.API_VIEW_CART : Urls.API_VIEW_WISH_LIST;
        final ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        OkHttpCalls calls = new OkHttpCalls(url, arrayKeyValueModel);
        calls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active)
                            progressBar.setVisibility(View.GONE);
                    }
                });
                FailureOccurred failureOccurred = new FailureOccurred(CartWishListActivity.this);
                failureOccurred.showFailureDialog(true);

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active)
                            progressBar.setVisibility(View.GONE);
                    }
                });
                if (response.isSuccessful()) {
                    try {
                        String res = Html.fromHtml(response.body().string()).toString();
                        Log.v("ResponseCart",res);
                        BookJsonParse jsonParse = new BookJsonParse(CartWishListActivity.this, comingFrom);
                        arrayBookModel = jsonParse.parseBookResponseGetArray(res);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (active) {
                                    adapter = new BookCartWishListAdapter(CartWishListActivity.this, arrayBookModel, comingFrom, active);
                                    recyclerView.setAdapter(adapter);
                                    decideTotal();
                                }
                            }
                        });
                    } catch (Exception e) {
                        Log.v("Exception", "" + e);
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialogMsg = new DialogMsg(CartWishListActivity.this);
                            dialogMsg.showDialog(getString(R.string.network_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogMsg.getDialog().dismiss();
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    public void decideTotal() {
        total = 0;
        int cartCount = 0;
        for (BookModel bookModel : arrayBookModel) {
            cartCount += 1;
            total += Float.parseFloat(bookModel.getBookDiscountedPrice());
        }
        txtTotalAmount.setText(getString(R.string.rs) + Math.round(total));


        if (cartCount == 0) {
            cardView.setVisibility(View.GONE);
            EmptyScreen emptyScreen = new EmptyScreen(this);
            emptyScreen.setIconAndMsg("No items in your cart yet", R.drawable.ic_shopping_cart_black_24dp);

            if (comingFrom.equals(getString(R.string.title_wish_list)))
                emptyScreen.setIconAndMsg("No items in your wish list yet", R.drawable.ic_favorite_border_white_24dp);

        } else {
            if (comingFrom.equals(getString(R.string.title_cart))) {
                cardView.setVisibility(View.VISIBLE);
                toolbar.setTitle(getString(R.string.title_cart) + "(" + arrayBookModel.size() + ")");
            } else
                toolbar.setTitle(getString(R.string.title_wish_list) + "(" + arrayBookModel.size() + ")");

        }

    }


}
