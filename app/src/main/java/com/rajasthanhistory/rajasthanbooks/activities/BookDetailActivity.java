package com.rajasthanhistory.rajasthanbooks.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.rajasthanhistory.rajasthanbooks.BookData.BookJsonParse;
import com.rajasthanhistory.rajasthanbooks.BookData.BookWishListOperation;
import com.rajasthanhistory.rajasthanbooks.BookData.CartBadgeCounter;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.adapter.BookDetailSliderAdapter;
import com.rajasthanhistory.rajasthanbooks.customViews.CustomTabLayout;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.dialogs.MyDialog;
import com.rajasthanhistory.rajasthanbooks.fragments.AuthorInfoFragment;
import com.rajasthanhistory.rajasthanbooks.fragments.BookDescriptionFragment;
import com.rajasthanhistory.rajasthanbooks.fragments.RatingReviewFragment;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.FailureOccurred;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;
import com.rajasthanhistory.rajasthanbooks.utils.ViewPagerAdapter;


import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class BookDetailActivity extends AppCompatActivity implements View.OnClickListener {

    CoordinatorLayout coordinatorLayout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Toolbar toolbar;
    AppBarLayout appBar;
    LinearLayout linLoading, linMainContent, linRating, linAddToCart, linView;
    RelativeLayout relBottomInner, relProgress;
    ImageView imgRating, imgFavorite, imgView;
    TextView txtTitle, txtAuthor, txtPrice, txtDiscountedPrice, txtAverageRating, txtSize, txtLanguage, txtView;
    ViewPager viewPager, viewPagerDescription;
    ImageView imgLeft, imgRight;

    TextView txtEBook;

    RelativeLayout relBadge;
    TextView txtPlus, txtMinus, txtQty, txtQtyValue, txtCartCounter;

    CustomTabLayout tabLayout;

    BookDetailSliderAdapter bookDetailSliderAdapter;

    String incomingBookId = "";
    int currentPositionBookImage = 0;

    User user;

    BookModel bookModel = new BookModel();

    boolean active;

    DialogMsg dialogMsg;

    CartBadgeCounter badgeCounter;
    public static BookDetailActivity bookDetailActivity;

    int countOfCartItems = 0;
    String path = "", fileName = "";
    ProgressBar progressDownload;
    TextView txtPercentage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);
        active = true;
        incomingBookId = getIntent().getExtras().getString("BookId");
        init();

        bookDetailActivity = this;

        toolbarOperation();
        onClickListeners();

        user = new UserDetails(this).getUserDetails();

        if (new ConnectionDetector(this).isConnectingToInternet()) {
            viewBookDetailOkHttp();
        } else {
            dialogMsg = new DialogMsg(this);
            dialogMsg.showDialog(getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogMsg.getDialog().dismiss();
                    finish();
                }
            });
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (new ConnectionDetector(this).isConnectingToInternet())
            okHttpViewCartForCounter();
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPositionBookImage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onStop() {
        active = false;
        super.onStop();
    }

    private void init() {
        appBar = (AppBarLayout) findViewById(R.id.appBar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtAuthor = (TextView) findViewById(R.id.txtAuthor);
        txtPrice = (TextView) findViewById(R.id.txtPrice);
        txtDiscountedPrice = (TextView) findViewById(R.id.txtDiscountedPrice);
        txtAverageRating = (TextView) findViewById(R.id.txtAverageRating);
        txtSize = (TextView) findViewById(R.id.txtSize);
        txtLanguage = (TextView) findViewById(R.id.txtLanguage);


        progressDownload = (ProgressBar) findViewById(R.id.progressDownload);
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);
        relProgress = (RelativeLayout) findViewById(R.id.relProgress);
        relProgress.setVisibility(View.GONE);

        LayerDrawable layerDrawable = (LayerDrawable) progressDownload.getProgressDrawable();
        Drawable progressDrawable = layerDrawable.findDrawableByLayerId(android.R.id.progress);
        progressDrawable.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_IN);

        imgRating = (ImageView) findViewById(R.id.imgRating);
        linRating = (LinearLayout) findViewById(R.id.linRating);
        linAddToCart = (LinearLayout) findViewById(R.id.linAddToCart);


        txtPlus = (TextView) findViewById(R.id.txtPlus);
        txtMinus = (TextView) findViewById(R.id.txtMinus);
        txtQty = (TextView) findViewById(R.id.txtQty);
        txtQtyValue = (TextView) findViewById(R.id.txtQtyValue);

        txtEBook = (TextView) findViewById(R.id.txtEBook);

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        viewPagerDescription = (ViewPager) findViewById(R.id.viewPagerDescription);
        tabLayout = (CustomTabLayout) findViewById(R.id.tabLayout);
        imgFavorite = (ImageView) findViewById(R.id.imgFavorite);

        imgLeft = (ImageView) findViewById(R.id.imgLeft);
        imgRight = (ImageView) findViewById(R.id.imgRight);

        linView = (LinearLayout) findViewById(R.id.linView);
        relBottomInner = (RelativeLayout) findViewById(R.id.relBottomInner);
        relBottomInner.setVisibility(View.GONE);
        linView.setVisibility(View.GONE);

        imgView = (ImageView) findViewById(R.id.imgView);
        txtView = (TextView) findViewById(R.id.txtView);

        linLoading = (LinearLayout) findViewById(R.id.linLoading);
        linMainContent = (LinearLayout) findViewById(R.id.linMainContent);
    }

    private void toolbarOperation() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("");
        collapsingToolbarLayout.setTitle("");
    }

    private void onClickListeners() {
        imgFavorite.setOnClickListener(this);
        imgLeft.setOnClickListener(this);
        imgRight.setOnClickListener(this);
        linAddToCart.setOnClickListener(this);
        txtPlus.setOnClickListener(this);
        txtMinus.setOnClickListener(this);
        linView.setOnClickListener(this);
    }


    private void viewBookDetailOkHttp() {
        linMainContent.setVisibility(View.GONE);
        linLoading.setVisibility(View.VISIBLE);

        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("product_id", incomingBookId));
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_BOOK_DETAIL, arrayKeyValueModel);
        final FailureOccurred failureOccurred = new FailureOccurred(this);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        linLoading.setVisibility(View.GONE);
                    }
                });
                failureOccurred.showFailureDialog(true);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    parseAndSetDetails(strResponse);
                }
            }
        });
    }

    public void parseAndSetDetails(final String strResponse) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (active) {

                        JSONObject json = new JSONObject(strResponse);
                        if (json.getInt("result") == 1) {
                            JSONObject jsonObject = json.getJSONObject("Message");
                            bookModel = new BookModel();
                            bookModel.setBookId(incomingBookId);
                            bookModel.setBookTitle(jsonObject.getString("product_name"));
                            bookModel.setBookDescription(jsonObject.getString("product_description"));
                            bookModel.setBookImage(jsonObject.getString("product_popimage"));
                            bookModel.setBookAuthorAbout(jsonObject.getString("about_author"));
                            bookModel.setBookAuthor(jsonObject.getString("author_name").toUpperCase());
                            bookModel.setBookPrice(jsonObject.getString("product_price"));
                            bookModel.setBookDiscountedPrice(jsonObject.getString("product_dis_price"));
                            bookModel.setBookSize(jsonObject.getString("product_length"));
                            bookModel.setBookLanguage(jsonObject.getString("language_name"));
                            bookModel.setBookUrlFileName(jsonObject.getString("upload_ebook"));

                            int discountedPrice = Integer.parseInt(bookModel.getBookPrice()) - Integer.parseInt(bookModel.getBookDiscountedPrice());
                            bookModel.setBookDiscountedPrice("" + discountedPrice);

                            bookModel.setBookType(jsonObject.getString("book_type"));
                            bookModel.setAvailableOrOutOfStock(jsonObject.getString("outofstock"));
                            bookModel.setBookAvgRating(Float.parseFloat(json.getJSONObject("rating").getString("rating")));

                            bookModel.setWished(json.getString("wishlist").equals("1"));

                            ArrayList<String> arrayBookImages = new ArrayList<>();
                            arrayBookImages.add(jsonObject.getString("product_popimage"));
                            arrayBookImages.add(jsonObject.getString("product_popimage1"));
                            arrayBookImages.add(jsonObject.getString("product_popimage2"));
                            arrayBookImages.add(jsonObject.getString("product_popimage3"));
                            arrayBookImages.add(jsonObject.optString("product_popimage4"));
                            arrayBookImages.add(jsonObject.optString("product_popimage5"));

                            bookModel.setArrayBookImageModel(arrayBookImages);

                            txtTitle.setText(bookModel.getBookTitle());
                            txtAuthor.setText("BY " + bookModel.getBookAuthor());

                            txtPrice.setText(getString(R.string.rs) + "" + bookModel.getBookPrice());
                            txtPrice.setPaintFlags(txtPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                            txtDiscountedPrice.setText(getString(R.string.rs) + "" + bookModel.getBookDiscountedPrice());

                            txtSize.setText("Size: " + bookModel.getBookSize());
                            txtLanguage.setText("Language: " + bookModel.getBookLanguage());

                            linRating.setVisibility(bookModel.getBookAvgRating() == 0 ? View.GONE : View.VISIBLE);
                            txtAverageRating.setText(bookModel.getBookAvgRating() + " (" + json.getInt("total_rating") + ")");

                            int color = bookModel.getBookAvgRating() >= 3 ? R.color.colorGreen : R.color.colorErrorRed;
                            imgRating.setColorFilter(ContextCompat.getColor(bookDetailActivity, color));

                            imgFavorite.setImageResource(bookModel.isWished() ? R.drawable.ic_favorite_white_24dp : R.drawable.ic_favorite_border_white_24dp);

                            bookDetailSliderAdapter = new BookDetailSliderAdapter(BookDetailActivity.this, arrayBookImages);
                            viewPager.setAdapter(bookDetailSliderAdapter);

                            hideShowTitle(bookModel.getBookTitle());

                            setUpTabDescription();

                            if (bookModel.getBookType().equalsIgnoreCase("1")) {
                                if (json.getString("purchase").equalsIgnoreCase("0")) {
                                    relBottomInner.setVisibility(View.VISIBLE);
                                    linView.setVisibility(View.GONE);
                                } else {
                                    relBottomInner.setVisibility(View.GONE);
                                    linView.setVisibility(View.VISIBLE);

                                    if (isDownloaded()) {
                                        txtView.setText(getString(R.string.title_view));
                                        imgView.setImageResource(R.drawable.ic_remove_red_eye_black_24dp);
                                    } else {
                                        txtView.setText(getString(R.string.title_download));
                                        imgView.setImageResource(R.drawable.ic_file_download_black_24dp);
                                    }

                                }
                            }
                            decideWhetherAvailableOrOutOfStock();

                        }
                        linLoading.setVisibility(View.GONE);
                        linMainContent.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    Log.v("ExceptionBookDetail", "" + e);
                }
            }
        });
    }

    private void hideShowTitle(final String bookTitle) {
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(bookTitle);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");//space is necessary
                    isShow = false;
                }
            }
        });
    }

    private void setUpTabDescription() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(BookDescriptionFragment.newInstance(bookModel), "Description");
        adapter.addFragment(AuthorInfoFragment.newInstance(bookModel), "Author Info");
        adapter.addFragment(RatingReviewFragment.newInstance(bookModel.getBookId()), "Reviews");
        viewPagerDescription.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPagerDescription);
    }

    private void decideWhetherAvailableOrOutOfStock() {
        //0-> Available   1->Out of Stock
        if (bookModel.getAvailableOrOutOfStock().equals("0")) {
            /*if (bookModel.getBookType().equals("1")) {
                txtQty.setText("Available");
                txtQty.setTextColor(ContextCompat.getColor(this, R.color.colorGreen));
                txtQtyValue.setVisibility(View.GONE);
                txtPlus.setVisibility(View.GONE);
                txtMinus.setVisibility(View.GONE);
            } else {
                txtQty.setTextColor(ContextCompat.getColor(this, R.color.colorDimPrimary));
                txtQty.setText("Qty");
                txtQtyValue.setVisibility(View.VISIBLE);
                txtPlus.setVisibility(View.VISIBLE);
                txtMinus.setVisibility(View.VISIBLE);
            }*/

            bookModel.setBookType("1");
            txtQty.setText("Available");
            txtQty.setTextColor(ContextCompat.getColor(this, R.color.colorGreen));
            txtQtyValue.setVisibility(View.GONE);
            txtPlus.setVisibility(View.GONE);
            txtMinus.setVisibility(View.GONE);
        } else {
            txtQtyValue.setVisibility(View.GONE);
            txtPlus.setVisibility(View.GONE);
            txtMinus.setVisibility(View.GONE);
            txtQty.setText("Out of stock");
            txtQty.setTextColor(ContextCompat.getColor(this, R.color.colorErrorRed));
            linAddToCart.setBackgroundResource(R.color.colorGreyDim);
            linAddToCart.setEnabled(false);
        }
    }

    private int plusMinusQty(boolean plus) {
        int count = Integer.parseInt(txtQtyValue.getText().toString().trim());
        if (plus) {
            count += 1;
        } else {
            if (count > 1) {
                count -= 1;
            }
        }
        return count;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (user.getUserId().trim().equalsIgnoreCase("0"))
            inflater.inflate(R.menu.menu_before_login, menu);
        else {
            inflater.inflate(R.menu.menu_main_after_login, menu);
            MenuItem menuItem = menu.findItem(R.id.itemCart);
            badgeCounter = new CartBadgeCounter(this, menuItem);
            badgeCounter.initiateView();
        }

        MenuItem menuSearch = menu.findItem(R.id.itemSearch);
        menuSearch.setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        Intent intentCartWishList = new Intent(BookDetailActivity.this, CartWishListActivity.class);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.login:
                intent = new Intent(BookDetailActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.register:
                intent = new Intent(BookDetailActivity.this, RegisterActivity.class);
                startActivity(intent);
                break;
            case R.id.itemMyProfile:
                intent = new Intent(BookDetailActivity.this, UserProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.itemLogOut:
                UserDetails userDetails = new UserDetails(this);
                userDetails.setUserDetails(new User());
                Toast.makeText(this, "Logged out successfully", Toast.LENGTH_LONG).show();

                if (MainActivity.mainActivity != null)
                    MainActivity.mainActivity.finish();

                intent = new Intent(BookDetailActivity.this, MainActivity.class);
                intent.putExtra("BookId", incomingBookId);
                startActivity(intent);
                finish();
                break;
            case R.id.itemWishList:
                intentCartWishList.putExtra("ComingFrom", getString(R.string.title_wish_list));
                startActivity(intentCartWishList);
                break;
            case R.id.itemOrders:
                intent = new Intent(this, MyOrdersActivity.class);
                startActivity(intent);
                break;
            case R.id.itemMyBooks:
                intent = new Intent(this, MyBooksActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isDownloaded() {
        path = Environment.getDataDirectory() + "/data/" + getPackageName() + "/files/";
        fileName = bookModel.getBookUrlFileName();
        File file = new File(path + fileName);
        return file.exists();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgFavorite:
                if (isUserLoggedIn()) {
                    if (new ConnectionDetector(this).isConnectingToInternet()) {
                        bookModel.setWished(!bookModel.isWished());
                        imgFavorite.setImageResource(bookModel.isWished() ? R.drawable.ic_favorite_white_24dp : R.drawable.ic_favorite_border_white_24dp);
                        if (bookModel.isWished()) {
                            BookWishListOperation operation = new BookWishListOperation(BookDetailActivity.this);
                            operation.addRemoveFavoriteOkHttp("", active, bookModel, imgFavorite);
                        } else {
                            BookWishListOperation operation = new BookWishListOperation(BookDetailActivity.this);
                            operation.addRemoveFavoriteOkHttp("1", active, bookModel, imgFavorite);
                        }
                    } else {
                        showInternetErrorDialog();
                    }

                } else {
                    showLogInDialog("You need to log in / register to add a book to your wish list");
                }
                break;
            case R.id.imgLeft:
                if (currentPositionBookImage > 0) {
                    currentPositionBookImage -= 1;
                    viewPager.setCurrentItem(currentPositionBookImage);
                }
                break;
            case R.id.imgRight:
                if (currentPositionBookImage < viewPager.getAdapter().getCount() - 1) {
                    currentPositionBookImage += 1;
                    viewPager.setCurrentItem(currentPositionBookImage);
                }
                break;
            case R.id.linAddToCart:
                if (isUserLoggedIn()) {
                    if (new ConnectionDetector(this).isConnectingToInternet()) {
                        addToCartOkHttp();
                    } else {
                        showInternetErrorDialog();
                    }
                } else {
                    showLogInDialog("You need to log in / register to add a book to your cart");
                }
                break;

            case R.id.txtPlus:
                int intQty = plusMinusQty(true);
                txtQtyValue.setText(intQty);
                break;
            case R.id.txtMinus:
                intQty = plusMinusQty(false);
                txtQtyValue.setText(intQty);
                break;
            case R.id.linView:
                if (isDownloaded()) {
                    /*Intent intent = new Intent(BookDetailActivity.this, MyPdfViewerActivity.class);
                    intent.putExtra(PdfViewerActivity.EXTRA_PDFFILENAME, path + fileName);
                    Log.v("Path+FileName",path+fileName);
                    startActivity(intent);*/
                    Intent intent=new Intent(this,ViewBookActivity.class);
                    intent.putExtra("BookModel",bookModel);
                    startActivity(intent);
                } else {
                    if (new ConnectionDetector(this).isConnectingToInternet()) {
                        new DownloadFileFromURL(relProgress, progressDownload, txtPercentage).execute(Urls.DOC_PRODUCT+bookModel.getBookUrlFileName());
                    } else {
                        dialogMsg = new DialogMsg(this);
                        dialogMsg.showDialog(getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogMsg.getDialog().dismiss();
                            }
                        });
                    }
                }
                break;
        }
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        ProgressBar progressBar;
        TextView txtPercentage;
        RelativeLayout relProgress;
        boolean error = false;

        public DownloadFileFromURL(RelativeLayout relProgress, ProgressBar progressBar, TextView txtPercentage) {
            this.relProgress = relProgress;
            this.progressBar = progressBar;
            this.txtPercentage = txtPercentage;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            linView.setEnabled(false);
            linView.setBackgroundResource(R.color.colorGreyDim);
            txtPercentage.setText("0%");
            progressBar.setProgress(0);
            relProgress.setVisibility(View.VISIBLE);
            error = false;
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                String strUrl = f_url[0];
                URL url = new URL(strUrl);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lengthOfFile = connection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);


                String fullPathWithName = path + fileName;
                File thisFile = new File(fullPathWithName);
                if (!thisFile.exists())
                    thisFile.getParentFile().mkdirs();

                // Output stream to write file
                OutputStream output = new FileOutputStream(fullPathWithName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error ", e.getMessage());
                error = true;
            }

            return null;
        }


        protected void onProgressUpdate(String... progress) {
            progressBar.setProgress(Integer.parseInt(progress[0]));
            txtPercentage.setText(progress[0] + "%");
        }

        @Override
        protected void onPostExecute(String file_url) {
            if (!error) {
                relProgress.setVisibility(View.GONE);
                linView.setEnabled(true);
                linView.setBackgroundResource(R.drawable.selector_accent_normal_rectangle);
                txtView.setText("View");
                imgView.setImageResource(R.drawable.ic_remove_red_eye_black_24dp);
            }else{
                Toast.makeText(BookDetailActivity.this,"Some server error occurred",Toast.LENGTH_LONG).show();
            }
        }

    }

    private void addToCartOkHttp() {
        linAddToCart.setEnabled(false);
        Snackbar.make(coordinatorLayout, "Adding to cart... Please wait", Snackbar.LENGTH_SHORT).show();
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        arrayKeyValueModel.add(new KeyValueModel("book_id", bookModel.getBookId()));
        arrayKeyValueModel.add(new KeyValueModel("price",bookModel.getBookDiscountedPrice()));

        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_ADD_TO_CART, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (active) {
                            linAddToCart.setEnabled(true);
                            Snackbar.make(coordinatorLayout, getString(R.string.network_error), Snackbar.LENGTH_SHORT).show();
                        }
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (response.isSuccessful()) {
                    String strResponse = response.body().string();
                    Log.v("ResponseAddCart", strResponse);
                    try {
                        JSONObject jsonObject = new JSONObject(strResponse);
                        countOfCartItems = jsonObject.getInt("Total");

                    } catch (Exception e) {
                        Log.v("ExceptionAddCart", "" + e);
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (countOfCartItems > 0) {
                                badgeCounter.setBadgeCount(countOfCartItems);
                                showConfirmCheckOutDialog();
                            }
                            linAddToCart.setEnabled(true);
                            Snackbar.make(coordinatorLayout, "Book successfully added to cart", Snackbar.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (active) {
                                linAddToCart.setEnabled(true);
                                Snackbar.make(coordinatorLayout, getString(R.string.network_error), Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
    }

    private void showConfirmCheckOutDialog() {
        final Dialog dialog = new MyDialog(this).getMyDialog(R.layout.dialog_confirm_checkout);
        TextView txtTitle, txtAuthor, txtAmount, txtBookType, txtContinue, txtCheckOut;
        ImageView imgBook;
        LinearLayout linParent;
        linParent = (LinearLayout) dialog.findViewById(R.id.linParent);
        txtTitle = (TextView) dialog.findViewById(R.id.txtTitle);
        txtAuthor = (TextView) dialog.findViewById(R.id.txtAuthor);
        txtAmount = (TextView) dialog.findViewById(R.id.txtAmount);
        txtBookType = (TextView) dialog.findViewById(R.id.txtBookType);
        txtContinue = (TextView) dialog.findViewById(R.id.txtContinue);
        txtCheckOut = (TextView) dialog.findViewById(R.id.txtCheckout);
        imgBook = (ImageView) dialog.findViewById(R.id.imgBook);

        Glide.with(this)
                .load(Urls.IMAGE_PRODUCT + bookModel.getBookImage())
                .thumbnail(0.2f)
                .override(getResources().getDimensionPixelSize(R.dimen.dim_250), getResources().getDimensionPixelSize(R.dimen.dim_300))
                .placeholder(R.drawable.ic_book_placeholder)
                .into(imgBook);

        txtTitle.setText(bookModel.getBookTitle());
        txtAuthor.setText(bookModel.getBookAuthor());
        txtAmount.setText(getString(R.string.rs) + bookModel.getBookDiscountedPrice());
        txtBookType.setText(bookModel.getBookType().equals("1") ? "E-book" : "Hard Copy");

        txtContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        txtCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(BookDetailActivity.this, CartWishListActivity.class);
                intent.putExtra("ComingFrom", getString(R.string.title_cart));
                startActivity(intent);
            }
        });

        linParent.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_up));
        dialog.show();

    }


    private boolean isUserLoggedIn() {
        user = new UserDetails(this).getUserDetails();
        return !user.getUserId().equals("0");
    }

    private void showLogInDialog(String message) {
        dialogMsg = new DialogMsg(this);
        dialogMsg.showDialog(message, R.drawable.ic_error_general, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.getDialog().dismiss();
            }
        });
    }

    private void showInternetErrorDialog() {
        dialogMsg = new DialogMsg(this);
        dialogMsg.showDialog(getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMsg.getDialog().dismiss();
            }
        });
    }

    private void okHttpViewCartForCounter() {
        if (!user.getUserId().trim().equals("0")) {
            String url = Urls.API_VIEW_CART;
            final ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
            arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
            OkHttpCalls calls = new OkHttpCalls(url, arrayKeyValueModel);
            calls.initiateCall(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        try {
                            String res = Html.fromHtml(response.body().string()).toString();
                            BookJsonParse jsonParse = new BookJsonParse(BookDetailActivity.this, getString(R.string.title_cart));
                            final ArrayList<BookModel> arrayBookModel = jsonParse.parseBookResponseGetArray(res);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (active)
                                        badgeCounter.setBadgeCount(arrayBookModel.size());
                                }
                            });
                        } catch (Exception e) {
                            Log.v("Exception", "" + e);
                        }
                    }
                }
            });
        }
    }
}
