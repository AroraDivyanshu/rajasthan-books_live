package com.rajasthanhistory.rajasthanbooks.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.rajasthanhistory.rajasthanbooks.BookData.EmptyScreen;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.adapter.BookCartWishListAdapter;
import com.rajasthanhistory.rajasthanbooks.custom.CustomFont;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etSearch;
    CardView cardSearch;
    ImageView imgBack, imgSearch;
    ProgressWheel progressSearch;

    ArrayList<BookModel> arrayBookSearchModel = new ArrayList<>();
    OkHttpCalls okHttpCalls;

    EmptyScreen emptyScreen;
    BookCartWishListAdapter adapter;
    RecyclerView recyclerView;
    boolean active;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        init();

        active = true;
        emptyScreen = new EmptyScreen(this);

        adapter = new BookCartWishListAdapter(this, arrayBookSearchModel, "Search", true);
        recyclerView.setAdapter(adapter);

        imgBack.setOnClickListener(this);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String strInput = editable.toString().trim();
                if (strInput.length() > 0) {
                    if (new ConnectionDetector(SearchActivity.this).isConnectingToInternet()) {
                        reset(true);
                        okHttpSearch(strInput);
                    }
                } else {
                    reset(true);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    protected void onStop() {
        active = false;
        super.onStop();

    }

    private void init() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        imgSearch = (ImageView) findViewById(R.id.imgSearch);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        cardSearch = (CardView) findViewById(R.id.cardSearch);
        etSearch = (EditText) findViewById(R.id.etSearch);
        etSearch.setTypeface(CustomFont.setFontRegular(getAssets()));

        progressSearch = (ProgressWheel) findViewById(R.id.progressBar);
        progressSearch.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void reset(boolean clear) {
        if (clear)
            arrayBookSearchModel.clear();

        if (okHttpCalls != null && okHttpCalls.getCall() != null)
            okHttpCalls.getCall().cancel();


        progressSearch.setVisibility(View.GONE);
        imgSearch.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();

        if (!clear && adapter.getItemCount() == 0) {
            emptyScreen.setIconAndMsg("No book found with this name", R.drawable.ic_search_black_24dp);
        }

        if (adapter.getItemCount() > 0) {
            emptyScreen.hideEmpty();
        }

    }

    private void okHttpSearch(final String strSearch) {

        reset(true);

        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("name", strSearch));

        progressSearch.setVisibility(View.VISIBLE);
        imgSearch.setVisibility(View.GONE);

        okHttpCalls = new OkHttpCalls(Urls.API_SEARCH, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    Log.v("SearchResponse", strResponse);
                    try {
                        JSONObject jsonObject = new JSONObject(strResponse);
                        if (jsonObject.getInt("result") == 1) {
                            JSONArray array = jsonObject.getJSONArray("Message");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject obj = array.getJSONObject(i);
                                BookModel model = new BookModel();
                                model.setBookId(obj.getString("product_id"));
                                model.setBookTitle(obj.getString("product_name"));
                                model.setBookPrice(obj.getString("product_price"));
                                model.setBookDiscountedPrice(obj.getString("product_dis_price"));

                                int discountedPrice = Integer.parseInt(model.getBookPrice()) - Integer.parseInt(model.getBookDiscountedPrice());
                                model.setBookDiscountedPrice("" + discountedPrice);

                                model.setBookAuthor(obj.getString("author"));
                                model.setBookType(obj.getString("book_type"));
                                model.setBookUrlFileName(obj.getString("upload_ebook"));
                                model.setBookImage(obj.getString("product_popimage"));
                                model.setCategoryName(obj.getString("category_name"));
                                model.setCategoryId(obj.getString("product_category"));
                                model.setBookSize(obj.getString("product_length"));
                                arrayBookSearchModel.add(model);
                            }
                        }
                    } catch (Exception e) {
                        Log.v("ExceptionSearch", "" + e);
                    }


                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressSearch.setVisibility(View.GONE);
                        imgSearch.setVisibility(View.VISIBLE);
                        reset(false);
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }
}
