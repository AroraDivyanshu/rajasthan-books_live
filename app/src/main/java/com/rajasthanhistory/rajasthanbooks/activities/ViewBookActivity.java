package com.rajasthanhistory.rajasthanbooks.activities;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.dialogs.MyDialog;
import com.rajasthanhistory.rajasthanbooks.model.BookIndex;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.GetSetSharedPrefs;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;
import com.rajasthanhistory.rajasthanbooks.utils.PreferencesKeys;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class ViewBookActivity extends AppCompatActivity {

    LinearLayout linParent;
    Toolbar toolbar;
    BookModel bookModel;

    PDFView pdfView;

    GetSetSharedPrefs prefs;
    DialogMsg dialogWait;

    ArrayList<BookIndex> arrayBookIndex = new ArrayList<>();

    String strResponse = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_book);
        init();
        bookModel = (BookModel) getIntent().getExtras().getSerializable("BookModel");
        toolbarOperation();

        prefs = new GetSetSharedPrefs(this, PreferencesKeys.PREFERENCE_SETTINGS);

        openPdf(0, true);

        loadIndexFirst();
    }

    private void init() {
        linParent = (LinearLayout) findViewById(R.id.linParent);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        pdfView = (PDFView) findViewById(R.id.pdfView);
    }

    private void toolbarOperation() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle(bookModel.getBookTitle());
    }

    private void openPdf(int pageNumber, boolean horizontal) {
        pdfView.fromUri(Uri.parse("/data/data/" + getPackageName() + "/files/" + bookModel.getBookUrlFileName()))
                .enableSwipe(true)
                .enableDoubletap(true)
                .swipeHorizontal(horizontal)
                .defaultPage(pageNumber)
                .enableAnnotationRendering(false)
                .password(null)
                .scrollHandle(new DefaultScrollHandle(this))
                .load();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_view_book, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.itemGoToPage:
                showDialogPageNumTitle("Go to page", 1);
                break;
            case R.id.itemIndex:
                showDialogPageNumTitle("Go to index", 2);
                break;
            case R.id.itemHorizontal:
                openPdf(pdfView.getCurrentPage(), true);
                break;
            case R.id.itemVertical:
                openPdf(pdfView.getCurrentPage(), false);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadIndexFirst() {
        prefs = new GetSetSharedPrefs(this, PreferencesKeys.PREFERENCE_SETTINGS);
        strResponse = prefs.getData(PreferencesKeys.PREF_BOOK_INDEX);
        if (new ConnectionDetector(this).isConnectingToInternet()) {
            okHttpViewIndex(bookModel.getBookId());
        } else {
            if (strResponse.trim().length() < 2) {
                Snackbar.make(linParent, getString(R.string.connection_error), Snackbar.LENGTH_LONG).show();
            } else {
                parseJsonResponseIndex();
            }
        }
    }

    private void okHttpViewIndex(String bookId) {
        dialogWait = new DialogMsg(this);
        dialogWait.showPleaseWaitDialog();
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("book_id", bookId));
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_VIEW_BOOK_INDEX, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                dialogWait.getDialog().dismiss();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    strResponse = Html.fromHtml(response.body().string()).toString();
                    prefs.putData(PreferencesKeys.PREF_BOOK_INDEX, strResponse);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            parseJsonResponseIndex();
                        }
                    });
                } else {
                    if (strResponse.trim().length() < 2) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Snackbar.make(linParent, "No index found on server database", Snackbar.LENGTH_LONG).show();
                            }
                        });
                    }
                }

                hideWaitDialog();
            }
        });
    }

    private void hideWaitDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialogWait.getDialog().dismiss();
            }
        });
    }

    private void parseJsonResponseIndex() {
        try {
            JSONObject json = new JSONObject(strResponse);
            if (json.getInt("result") == 1) {
                arrayBookIndex.clear();
                JSONArray array = json.getJSONArray("Message");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    BookIndex bookIndex = new BookIndex();
                    bookIndex.setPageNumber(Integer.parseInt(obj.getString("indexing_pageno")));
                    bookIndex.setTitle(obj.getString("indexing_title"));
                    arrayBookIndex.add(bookIndex);
                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Snackbar.make(linParent, "No index found for this e-book", Snackbar.LENGTH_LONG).show();
                    }
                });
            }
        } catch (Exception e) {

        }

        if (dialogWait != null && dialogWait.getDialog() != null && dialogWait.getDialog().isShowing())
            dialogWait.getDialog().dismiss();

    }

    //pageNumOrIndex=> 1 PageNum   2 Index
    private void showDialogPageNumTitle(String title, final int pageNumOrIndex) {
        final Dialog dialog = new MyDialog(this).getMyDialog(R.layout.dialog_single_list);
        TextView txtTitle;
        ListView listView;
        txtTitle = (TextView) dialog.findViewById(R.id.txtTitle);
        listView = (ListView) dialog.findViewById(R.id.listView);
        txtTitle.setText(title);

        ArrayList<String> arrayPage = new ArrayList<>();

        if (pageNumOrIndex == 1) {
            for (int i = 0; i < pdfView.getPageCount(); i++)
                arrayPage.add("" + (i + 1));
        } else {
            for (BookIndex bookIndex : arrayBookIndex)
                arrayPage.add(bookIndex.getTitle());
        }

        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.row_text_view_page_num, arrayPage);
        listView.setAdapter(adapter);

        if (listView != null) {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    dialog.dismiss();
                    pdfView.jumpTo(pageNumOrIndex == 1 ? i : arrayBookIndex.get(i).getPageNumber());

                    //pdfView.loadPages();
                }
            });
        }

        if (pageNumOrIndex == 1) {
            if (arrayPage.size() > 0)
                dialog.show();
            else
                Snackbar.make(linParent, "Some error occurred", Snackbar.LENGTH_LONG).show();
        } else {
            if (arrayPage.size() > 0)
                dialog.show();
            else
                Snackbar.make(linParent, "No index found for this e-book", Snackbar.LENGTH_LONG).show();
        }

    }
}
