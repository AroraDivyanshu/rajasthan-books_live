package com.rajasthanhistory.rajasthanbooks.activities;

import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.graphics.pdf.PdfRenderer;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.rajasthanhistory.rajasthanbooks.R;
import com.rajasthanhistory.rajasthanbooks.adapter.OrderDetailBookAdapter;
import com.rajasthanhistory.rajasthanbooks.dialogs.DialogMsg;
import com.rajasthanhistory.rajasthanbooks.model.BookModel;
import com.rajasthanhistory.rajasthanbooks.model.KeyValueModel;
import com.rajasthanhistory.rajasthanbooks.url.Urls;
import com.rajasthanhistory.rajasthanbooks.userModel.User;
import com.rajasthanhistory.rajasthanbooks.userModel.UserDetails;
import com.rajasthanhistory.rajasthanbooks.utils.ConnectionDetector;
import com.rajasthanhistory.rajasthanbooks.utils.FailureOccurred;
import com.rajasthanhistory.rajasthanbooks.utils.GetSetSharedPrefs;
import com.rajasthanhistory.rajasthanbooks.utils.OkHttpCalls;
import com.rajasthanhistory.rajasthanbooks.utils.PayPalConfig;
import com.rajasthanhistory.rajasthanbooks.utils.PreferencesKeys;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MakePaymentActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recyclerViewBooks;
    Toolbar toolbar;
    TextView txtTotalAmount, txtPayPal, txtPayTm;
    LinearLayout linDetail, llPaymentThrough;
    ImageView imgExpandCollapse;
    boolean expanded = false;
    Button btnBuyNow;


    ArrayList<BookModel> arrayBookModel = new ArrayList<>();
    OrderDetailBookAdapter adapter;

    DialogMsg dialogMsg;
    User user;

    public static final int PAYPAL_REQUEST_CODE = 123;

    public static final String API_CURRENCY_CONVERT = "http://api.fixer.io/latest?symbols=USD,INR&base=INR";


    //Paypal Configuration Object
    private static PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfig.PAYPAL_ENVIRONMENT)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID_LIVE);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_payment);
        init();

        user = new UserDetails(this).getUserDetails();

        toolbarOperation();
        getBookDetails();
        onClickListeners();

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

    }


    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtTotalAmount = (TextView) findViewById(R.id.txtTotalAmount);
        txtPayPal = (TextView) findViewById(R.id.txtPayPal);
        txtPayTm = (TextView) findViewById(R.id.txtPayTm);
        linDetail = (LinearLayout) findViewById(R.id.linDetail);
        btnBuyNow = (Button) findViewById(R.id.btnBuyNow);
        llPaymentThrough = (LinearLayout) findViewById(R.id.llPaymentThrough);
        imgExpandCollapse = (ImageView) findViewById(R.id.imgExpandCollapse);
        recyclerViewBooks = (RecyclerView) findViewById(R.id.recyclerViewBooks);

        recyclerViewBooks.setLayoutManager(new LinearLayoutManager(this));
    }

    private void toolbarOperation() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Check out");
    }

    private void onClickListeners() {
        linDetail.setOnClickListener(this);
        txtPayPal.setOnClickListener(this);
        txtPayTm.setOnClickListener(this);
        btnBuyNow.setOnClickListener(this);
    }

    private void getBookDetails() {
        arrayBookModel = (ArrayList<BookModel>) getIntent().getExtras().getSerializable("ArrayBookModel");
        adapter = new OrderDetailBookAdapter(this, arrayBookModel, "CheckOut");
        recyclerViewBooks.setAdapter(adapter);

        txtTotalAmount.setText(getString(R.string.rs) + getTotal() + " ( " + arrayBookModel.size() + " )");

        if (getTotal() == 0) {
            llPaymentThrough.setVisibility(View.GONE);
            btnBuyNow.setVisibility(View.VISIBLE);
        } else {
            llPaymentThrough.setVisibility(View.VISIBLE);
            btnBuyNow.setVisibility(View.GONE);
        }

    }

    private int getTotal() {
        int total = 0;
        for (BookModel bookModel : arrayBookModel)
            total += Integer.parseInt(bookModel.getBookDiscountedPrice());
        return total;
    }

    /*private String getBookDetailJsonArray() {
        try {
            JSONArray array = new JSONArray();
            for (BookModel bookModel : arrayBookModel) {
                JSONObject object = new JSONObject();
                object.put("book_id", bookModel.getBookId());
                object.put("book_title", bookModel.getBookTitle());
                object.put("book_image", bookModel.getBookImage());
                object.put("book_description", bookModel.getBookDescription());
                object.put("book_author_name", bookModel.getBookAuthor());
                object.put("book_price", bookModel.getBookPrice());
                object.put("book_discounted_price", bookModel.getBookDiscountedPrice());
                object.put("book_ebook_url", bookModel.getBookUrlFileName());
                object.put("book_size", bookModel.getBookSize());
                object.put("book_qty", "1");
                object.put("book_type", "1");
                array.put(object);
            }
            return array.toString();
        } catch (Exception e) {

        }
        return "";
    }*/

    //PayTm
    private PayPalPayment prepareFinalCart() {

        PayPalItem[] items = new PayPalItem[arrayBookModel.size()];
        items = arrayBookModel.toArray(items);

        // Total amount
        BigDecimal subtotal = PayPalItem.getItemTotal(items);

        // If you have shipping cost, add it here
        BigDecimal shipping = new BigDecimal("0.0");

        // If you have tax, add it here
        BigDecimal tax = new BigDecimal("0.0");

        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(
                shipping, subtotal, tax);

        BigDecimal amount = subtotal.add(shipping).add(tax);

        PayPalPayment payment = new PayPalPayment(
                amount,
                PayPalConfig.DEFAULT_CURRENCY,
                "Description about transaction. This will be displayed to the user.",
                PayPalConfig.PAYMENT_INTENT);

        payment.items(items).paymentDetails(paymentDetails);

        // Custom field like invoice_number etc.,
        payment.custom("This is text that will be associated with the payment that the app can use.");

        return payment;
    }

    private String getCartIds() {
        String cartIds = "";
        for (BookModel bookModel : arrayBookModel)
            cartIds += bookModel.getCartId() + ",";

        cartIds = cartIds.substring(0, cartIds.length() - 1);
        return cartIds;
    }

    private void okHttpPlaceOrder(String payPalJson, String paymentGateway) {
        dialogMsg = new DialogMsg(this);
        dialogMsg.showPleaseWaitDialog();
        user = new UserDetails(this).getUserDetails();
        Log.v("UserDetailInOrder", "" + user.getUserId() + "_" + user.getUserFirstName() + "_" + user.getUserEmail());
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        arrayKeyValueModel.add(new KeyValueModel("total_amout", getTotal() + ""));
        arrayKeyValueModel.add(new KeyValueModel("flag", "1")); //1-> order from android app
        arrayKeyValueModel.add(new KeyValueModel("payment_type", "1"));
        arrayKeyValueModel.add(new KeyValueModel("payment_gateway", paymentGateway));
        Log.v("CartIds", getCartIds());
        arrayKeyValueModel.add(new KeyValueModel("book_detail", getCartIds()));
        arrayKeyValueModel.add(new KeyValueModel("user_name", user.getUserFirstName()));
        arrayKeyValueModel.add(new KeyValueModel("user_email", user.getUserEmail()));
        arrayKeyValueModel.add(new KeyValueModel("paypal_json", payPalJson));

        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_PLACE_ORDER, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogMsg.getDialog().dismiss();
                    }
                });
                FailureOccurred failureOccurred = new FailureOccurred(MakePaymentActivity.this);
                failureOccurred.showFailureDialog(false);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogMsg.getDialog().dismiss();
                    }
                });
                if (response.isSuccessful()) {
                    final String strResponse = Html.fromHtml(response.body().string()).toString();
                    Log.v("Response", strResponse);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject json = new JSONObject(strResponse);
                                if (json.getInt("result") == 1) {
                                    String orderCodeName = json.getString("order_name");
                                    okHttpViewUserBooks();
                                    dialogMsg = new DialogMsg(MakePaymentActivity.this);
                                    dialogMsg.showDialog("Your order successfully placed\nYour order id is: " + orderCodeName, R.drawable.ic_success, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialogMsg.getDialog().dismiss();

                                            if (BookDetailActivity.bookDetailActivity != null)
                                                BookDetailActivity.bookDetailActivity.finish();

                                            if (CartWishListActivity.cartWishListActivity != null)
                                                CartWishListActivity.cartWishListActivity.finish();

                                            finish();
                                        }
                                    });
                                }
                            } catch (Exception e) {

                            }
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialogMsg = new DialogMsg(MakePaymentActivity.this);
                            dialogMsg.showDialog(getString(R.string.network_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogMsg.getDialog().dismiss();
                                }
                            });
                        }
                    });

                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linDetail:
                expanded = !expanded;
                recyclerViewBooks.setVisibility(expanded ? View.VISIBLE : View.GONE);
                imgExpandCollapse.setImageResource(expanded ? R.drawable.ic_keyboard_arrow_up_black_24dp : R.drawable.ic_keyboard_arrow_down_black_24dp);
                break;
            case R.id.txtPayPal:
                makePayment("PayPal");
                break;
            case R.id.txtPayTm:
                makePayment("PayTm");
                break;
            case R.id.btnBuyNow:
                okHttpPlaceOrder("","Free");
                break;
        }
    }

    //PayPal or PayTm
    private void makePayment(String payPalTm) {
        if (!new ConnectionDetector(this).isConnectingToInternet()) {
            dialogMsg = new DialogMsg(this);
            dialogMsg.showDialog(getString(R.string.connection_error), R.drawable.ic_error_internet, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogMsg.getDialog().dismiss();
                }
            });
            return;
        }

        switch (payPalTm) {
            case "PayPal":
                okHttpCurrencyConvert();
                break;
            case "PayTm":
                okHttpPlaceOrder("", "PayTm");
                break;
        }

    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    private void makePaymentFromPayPal(String paymentAmount) {


        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf("0.02")), PayPalConfig.DEFAULT_CURRENCY, getString(R.string.app_name),
                PayPalConfig.PAYMENT_INTENT);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    private void okHttpCurrencyConvert() {
        int total = 0;
        final DialogMsg dialogMsg = new DialogMsg(this);
        dialogMsg.showPleaseWaitDialog();


        OkHttpCalls okHttpCalls = new OkHttpCalls(API_CURRENCY_CONVERT, new ArrayList<KeyValueModel>());
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogMsg.getDialog().dismiss();
                    }
                });
                FailureOccurred failureOccurred = new FailureOccurred(MakePaymentActivity.this);
                failureOccurred.showFailureDialog(false);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        String strResponse = response.body().string();
                        JSONObject json = new JSONObject(strResponse);
                        float flTotal = Float.parseFloat(json.getJSONObject("rates").getString("USD"));
                        final float total = getTotal() * flTotal;
                        Log.v("FlTotal and total", flTotal + "\n" + total);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dialogMsg.getDialog().dismiss();
                                makePaymentFromPayPal(total + "");
                            }
                        });

                    } catch (Exception e) {

                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);
                        JSONObject json = new JSONObject(paymentDetails);
                        JSONObject jsonObject = json.getJSONObject("response");
                        String state = jsonObject.getString("state");
                        String payId = jsonObject.getString("id");
                        if (state.equalsIgnoreCase("approved")) {
                            okHttpPlaceOrder(jsonObject.toString(), "PayPal");
                        }

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                        dialogMsg = new DialogMsg(MakePaymentActivity.this);
                        dialogMsg.showDialog("Some unknown error occurred", R.drawable.ic_error_general, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogMsg.getDialog().dismiss();
                            }
                        });
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    private void okHttpViewUserBooks() {
        ArrayList<KeyValueModel> arrayKeyValueModel = new ArrayList<>();
        arrayKeyValueModel.add(new KeyValueModel("user_id", user.getUserId()));
        OkHttpCalls okHttpCalls = new OkHttpCalls(Urls.API_VIEW_MY_BOOKS, arrayKeyValueModel);
        okHttpCalls.initiateCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String strResponse = Html.fromHtml(response.body().string()).toString();
                    GetSetSharedPrefs prefs = new GetSetSharedPrefs(MakePaymentActivity.this, PreferencesKeys.PREFERENCE_SETTINGS);
                    prefs.putData(PreferencesKeys.PREF_MY_BOOKS, strResponse);
                }
            }
        });
    }
}
