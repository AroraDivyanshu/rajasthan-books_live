package com.rajasthanhistory.rajasthanbooks.custom;

import android.content.res.AssetManager;
import android.graphics.Typeface;

/**
 * Created by wscubetech on 1/4/16.
 */
public class CustomFont {

    public static final Typeface setFontRegular(AssetManager assetManager) {
        return Typeface.createFromAsset(assetManager, "fonts/montserrat_light.otf");
    }

    public static final Typeface setFontBold(AssetManager assetManager) {
        return Typeface.createFromAsset(assetManager, "fonts/montserrat_regular.otf");
    }


}
