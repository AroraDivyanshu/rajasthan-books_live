package com.rajasthanhistory.rajasthanbooks.customViews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;
import android.widget.EditText;

import com.rajasthanhistory.rajasthanbooks.custom.CustomFont;

/**
 * Created by SR on 9/21/2016.
 */
@SuppressLint("AppCompatCustomView")
public class MyEditTextBold extends EditText {

    public MyEditTextBold(Context context) {
        super(context);
        init();
    }

    public MyEditTextBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyEditTextBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        Typeface tf = CustomFont.setFontBold(getContext().getAssets());
        setTypeface(tf);
    }
}
