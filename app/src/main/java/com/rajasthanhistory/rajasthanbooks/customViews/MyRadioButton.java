package com.rajasthanhistory.rajasthanbooks.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.rajasthanhistory.rajasthanbooks.custom.CustomFont;

/**
 * Created by wscubetech on 23/10/16.
 */
public class MyRadioButton extends android.support.v7.widget.AppCompatRadioButton {
    public MyRadioButton(Context context) {
        super(context);
        init();
    }

    public MyRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        Typeface tf = CustomFont.setFontRegular(getContext().getAssets());
        setTypeface(tf);
    }
}
