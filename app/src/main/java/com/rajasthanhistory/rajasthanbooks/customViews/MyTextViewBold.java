package com.rajasthanhistory.rajasthanbooks.customViews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.rajasthanhistory.rajasthanbooks.custom.CustomFont;


/**
 * Created by wscubetech on 25/5/16.
 */
@SuppressLint("AppCompatCustomView")
public class MyTextViewBold extends TextView {

    public MyTextViewBold(Context context) {
        super(context);
        init();
    }

    public MyTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        Typeface tf = CustomFont.setFontBold(getContext().getAssets());
        setTypeface(tf, 1);
    }
}
